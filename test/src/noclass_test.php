<?php

// For access check
defined('_TEST') or die;

function vuln_sqli($input) {
	antiCSRF();
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $input);
	$db->query();
}

function vuln_sqli2($input) {
    antiCSRF();

    $query->clear()
        ->select('t.*')
        ->from($db->quoteName('#__finder_taxonomy') . ' AS t')
        ->where('t.parent_id = ' . (int) $bk)
        ->where('t.state = 1')
        ->where('t.access IN (' . $input . ')')
        ->order('t.ordering, t.title');
}

function vuln_sqli3($input) {
    antiCSRF();

    if ($authorId)
    {
        $authorWhere = 'a.created_by ' . $type . (int) $authorId;
    }
    elseif (is_array($authorId))
    {
        $authorWhere = 'a.created_by ' . $type . ' (' . $authorId[0] . ')';
    }

    $db = $this->getDb();
    $db->select('SELECT *');
    $db->from('blahh');
    $db->where($authorWhere);
    $db->query();
}

function notvuln_sqli1($input) {
	antiCSRF();
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . new TestClass()->dbQuote($input));
	$db->query();
}

function notvuln_sqli2($input) {
	antiCSRF();
	
	$cast_input = (int) $input;
	$other_cast_input = $cast_input;
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $cast_input);
	$db->query();
}

function notvuln_sqli2a($input) {
	antiCSRF();
	
	$cast_input = (int) $input;
	$other_cast_input = $cast_input;
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $other_cast_input);
	$db->query();
}

function notvuln_sqli2b($input) {
	antiCSRF();
	
	$cast_input = (int) $input;
	$other_cast_input = $cast_input;
	$another_cast_input = $other_cast_input;
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $another_cast_input);
	$db->query();
}

function notvuln_sqli2c($input) {
	antiCSRF();
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . (int) $input);
	$db->query();
}

function notvuln_sqli2d($items) {
	antiCSRF();
	
	foreach ($items as $item)
	{
		$bid[] = (int) $item->id;
	}
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . implode(',', $bid));
	$db->query();
}

function notvuln_sqli2e($input) {
	antiCSRF();
	
	$arr['test'] = (int) $input;
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $arr['test']);
	$db->query();
}

function notvuln_sqli2f($input) {
	antiCSRF();
	
	$obj->test = (int) $input;
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $obj->test);
	$db->query();
}

function notvuln_sqli3($input) {
	antiCSRF();
	
	if (is_numeric($input)) {
		$val_input = $input;
	} else {
		$val_input = 1;
	}
	
	$db = getDb();
	$db->select('SELECT *');
	$db->from('blahh');
	$db->where('col = ' . $val_input);
	$db->query();
}

function notvuln_sqli3a($input) {
    $this->antiCSRF();

    if (!is_numeric($input)) {
        return;
    }

    $query->clear()
        ->select('t.*')
        ->from($db->quoteName('#__finder_taxonomy') . ' AS t')
        ->where('t.parent_id = ' . (int) $bk)
        ->where('t.state = 1')
        ->where('t.access IN (' . $input . ')')
        ->order('t.ordering, t.title');
}