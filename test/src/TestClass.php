<?php 

class TestClass {

    private $test = 2;
	
	public function __construct() {
		// For access check
		defined('_TEST') or die;
	}
	
	private function antiCSRF() {
		return false;
	}
	
	private function getDb() {
		return $db;
	}
	
	private function dbQuote($unquoted) {
		return $quoted;
	}
	
	public function vuln_xss() {
		$this->antiCSRF();
		
		echo $_GET['foo'];
	}
	
	public function vuln_include() {
		$this->antiCSRF();
		
		include($var);
	}
	
	public function vuln_danger() {
		$this->antiCSRF();
		
		exec('ls -la');
	}
	
	public function vuln_csrf() {
		echo "Hello";
	}
	
	public function vuln_sqli($input) {
		$this->antiCSRF();
		
		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $input);
		$db->query();
	}

	public function vuln_sqli2($input) {
		$this->antiCSRF();

		$query->clear()
		    ->select('t.*')
		    ->from($db->quoteName('#__finder_taxonomy') . ' AS t')
			->where('t.parent_id = ' . (int) $bk)
			->where('t.state = 1')
			->where('t.access IN (' . $input . ')')
			->order('t.ordering, t.title');
	}

	public function vuln_sqli3($input) {
	    $this->antiCSRF();

	    if ($input)
		{
			$authorWhere = 'a.created_by ' . $type . (int) $input;
		}
		elseif (is_array($input))
		{
			$authorWhere = 'a.created_by ' . $type . ' (' . $input[0] . ')';
		}

		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where($authorWhere);
		$db->query();
	}
	
	public function notvuln_sqli1($input) {
		$this->antiCSRF();
	
		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $this->dbQuote($input));
		$db->query();
	}
	
	public function notvuln_sqli2($input) {
		$this->antiCSRF();
		
		$cast_input = (int) $input;
		$other_cast_input = $cast_input;
	
		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $cast_input);
		$db->query();
	}
	
	public function notvuln_sqli2a($input) {
		$this->antiCSRF();
	
		$cast_input = (int) $input;
		$other_cast_input = $cast_input;
	
		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $other_cast_input);
		$db->query();
	}
	
	public function notvuln_sqli2b($input) {
		$this->antiCSRF();
	
		$cast_input = (int) $input;
		$other_cast_input = $cast_input;
		$another_cast_input = $other_cast_input;
	
		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $another_cast_input);
		$db->query();
	}

	public function notvuln_sqli2c($input) {
		$this->antiCSRF();

		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . (int) $input);
		$db->query();
	}

	public function notvuln_sqli2d($items) {
		$this->antiCSRF();

		foreach ($items as $item)
		{
			$bid[] = (int) $item->id;
		}

		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . implode(',', $bid));
		$db->query();
	}

	public function notvuln_sqli2e($input) {
		$this->antiCSRF();

		$arr['test'] = (int) $input;

		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $arr['test']);
		$db->query();
	}

	public function notvuln_sqli2f($input) {
		$this->antiCSRF();

		$obj->test = (int) $input;

		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $obj->test);
		$db->query();
	}
	
	public function notvuln_sqli3($input) {
		$this->antiCSRF();
	
		if (is_numeric($input)) {
			$val_input = $input;
		} else {
			$val_input = 1;
		}
	
		$db = $this->getDb();
		$db->select('SELECT *');
		$db->from('blahh');
		$db->where('col = ' . $val_input);
		$db->query();
	}

	public function notvuln_sqli3a($input) {
		$this->antiCSRF();

		if (!is_numeric($input)) {
            return;
		}

		$query->clear()
		    ->select('t.*')
		    ->from($db->quoteName('#__finder_taxonomy') . ' AS t')
			->where('t.parent_id = ' . (int) $bk)
			->where('t.state = 1')
			->where('t.access IN (' . $input . ')')
			->order('t.ordering, t.title');
	}
	
}
