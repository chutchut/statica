import pytest

import statica


@pytest.fixture
def proj_name():
    return 'Test'

@pytest.fixture
def statica_instance(proj_name):
    instance = statica.get_instance(project=proj_name)
    instance.reloadProject(proj_name)
    instance.showErrors = True
    return instance

def test_basic_checks(statica_instance, proj_name):
    statica_instance.run()
    rem_issues = statica_instance.getRemainingIssues(proj_name)
    
    assert len(rem_issues['access']) == 1
    assert len(rem_issues['danger']) == 1
    assert len(rem_issues['includes']) == 1
    assert len(rem_issues['csrf']) == 1
    assert len(rem_issues['sqli']) == 6
    assert len(rem_issues['xss']) == 1
    assert len(statica_instance.getErrors(proj_name, 'STANDALONE')) == 0
