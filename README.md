# Statica #

Statica is a tool to identify potential security issues in large PHP open-source projects. The idea is to aid the user in manual review of the source code by identifying interesting areas of code to analyse.

The tool saves results to an SQLite database and compares the results found between tags to identify new or fixed issues.

### How-to: Summary ###

* Install dependencies
* Setup global config
* Setup project config
* Create project class
* Define project checks

### How-to ###

#### Install dependencies ####

`pip install phply blessings`

#### Setup global config ####

Sample:

```
[Global]
git_root_path: /opt/lampp/htdocs/statica
sync_freq: 5m
db_path: /home/chutchut/git/statica/conf/statica.db
```

Parameters:

* git_root_path - Path where projects will be cloned.
* sync_freq - Global synchronisation frequency 
* db_path - Path to the SQLite database file. Will be created if it doesn't exist.

#### Setup project config ####

Sample:

```
[JoomlaProject]
name: Joomla
tagstart: 3.1.0
comstart: None
clone_url: https://github.com/joomla/joomla-cms.git
commit_branch: staging
check_freq: tag
sync_freq: 1m
check: danger,includes,csrf,xss,sqli
tag_filter: ^\d+\.\d+\.\d+$
```

Parameters:

* name - Name of the project
* tagstart - Which tag to start analysis on (overridden by CLI parameter --start)
* comstart - Which commit to start analysis on (overridden by CLI parameter --start)
* clone_url - Public clone URL of the project (must not require auth)
* commit_branch - Branch on which to do analysis of commits
* check_freq - Analysis mode (tag/commit). Currently tag is only supported mode
* sync_freq - Project synchronisation frequency
* check - CSV list of checks to perform
* tag_filter - Regular expression used to filter tags

#### Create project class ####

Example:

```python
class JoomlaProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['tests', 'build', 'vendor'])
        self.inputs.extend([('func', 'get', '*input'), ('func', 'get', '*get'), ('func', 'get', '*post'), ('func', 'getState', None), ('func', 'getUserState', None), ('func', 'getUserStateFromRequest', None)])
        self.fMatch = ['controller', 'controllers', 'controller.php', 'components', 'models', 'helpers', 'views']
        self.cMatch = ['controller', 'controllers', 'controller.php']
        self.checkConf.extend([
            {'includes': {'filter': ['%s/components' % (self.path)], 'check_parent': 0}},
            {'access': {'filter': ['administrator/components'], 'check_parent': 0}},
            {'csrf': {'filter': self.cMatch, 'check_parent': 1}},
            {'xss': {'filter': self.cMatch + ['components'], 'check_parent': 0}},
            {'sqli': {'filter': self.cMatch + ['models', 'helpers', 'views', 'libraries', 'plugins', 'modules'], 'check_parent': 0}},
        ])
```

Constructor:

* Sub-class (inherit) StaticaProject object for the project class.
* `self.excludedPaths.extend([])` - Add a list of strings to be excluded from the paths examined.
* `self.inputs.extend([])` - Define inputs for the project. I.e. functions which take input from the user.
* `self.fMatch` - Use these strings to include in the search of the source tree.
* `self.cMatch` - Use these strings to match controller code in MVC-based projects.
* `self.checkConf.extend([])` - Parameter for check-specific config, for example the file input filter and check_parent directive.

#### Define project checks ####

The checks run against the project code are generally created by the user, and revolve around identifying whether specific dangerous or validation functions are used in the code. The presence (or absence) of such functions is used to identify interesting points in the source code to be flagged for further manual review. All checks must be defined in sub-classes of the StaticaProject object, to gain access to the below helper functions.

Two generic (i.e common for all projects) checks are the dangerous functions check and the dynamic includes check. The dangerous function check searches the code for pre-defined 'dangerous' functions:

```python
def check_danger(self, path, cls, fnodes):
    for fnc in self.dangerFuncs:
        if self.hasFunction(fnodes, fnc):
            self.addResult((fnc, cls, path))
```

The dynamic includes check searches the code for include/include_once and require/require_once calls that take a variable or method call as a parameter:

```python
def check_includes(self, path, cls, fnodes):
    incs = self.getAllNodes(Include, fnodes)
    reqs = self.getAllNodes(Require, fnodes)
    for inc in incs:
        t = type(inc.expr)
        if t is Variable or t is MethodCall or t is StaticMethodCall or t is FunctionCall or t is ObjectProperty:
            # Dynamic 
            if inc.once:
                fname = 'include_once'
            else:
                fname = 'include'
            self.addResult((fname, cls, path))
    for req in reqs:
        t = type(req.expr)
        if t is Variable or t is MethodCall or t is StaticMethodCall or t is FunctionCall or t is ObjectProperty:
            # Dynamic 
            if req.once:
                fname = 'require_once'
            else:
                fname = 'require'
            self.addResult((fname, cls, path))
```
All checks are passed three parameters: path (current file path), cls (current class name or 'n/a') and fnodes (nodes related to current function).

Helper functions summary:

* `getAllNodes(self, obj, node)` - returns a list of nodes that match the object type passed in.
* `hasFuncModifiers(modifiers, function)` - returns boolean. True if passed function has specific modifier.
* `getFunctions(self, nodes, name, cont=None, inst=True)` - returns list of function nodes dependant on parameters. Empty list if not found.
* `getFunction(self, nodes, name, param, cont=None, inst=True)` - returns function node dependant on parameters. None if not found.
* `hasFunction(self, nodes, name, cont=None, inst=True)` - returns boolean. True if function found in node list.
* `getUserInputs(self, nodes, inputs)` - returns list of user input nodes, dependant of defined inputs passed in the constructor. 
* `addResult(self, resTuple)` - Add a result to the list for the check where the call was made. Will automatically check for and avoid duplicate results. 

Context:

In Statica, there is a notion of 'context' when dealing with nodes produced from the parsing and tokenisation of source code. The 'inner' context refers to nodes that reside within a class, that is, all properties and functions within the class's scope. The 'outer' context refers to everything outside of the class scope. One file may have both an inner and outer context whose nodes should be examined, but often we can choose a context to tune our checks. For example, for a CMS which uses object-based controllers we want to check, we can be sure the interesting logic will always be within the class, so we can choose the inner context. To define this in a check do the following:

```python
def check_csrf(self, path, cls, fnodes):
    if self.context == 'outer':
        return
    ...
```

Note: when in  the inner context, the fnodes.name variable will be set, defining the name of the current function.

Check summary:

* Nodes to examine are delivered to the check based on the check input file filter defined in the constructor.
* Class variable will either be the current class or 'n/a'
* Generate results by creating a 3-item tuple:
    - Item 1: Name of the interesting function.
    - Item 2: Class containing function (only available in inner/class context), or 'n/a'.
    - Item 3: Path to the file where the result was identified.
* Add a result using self.addResult()

### Usage ###

```
usage: statica.py [-h] [--conf CONF] [--mode MODE] [--report]
                  [--project PROJECT] [--start START] [--reload]
                  [--check CHECK]

optional arguments:
  -h, --help         show this help message and exit
  --conf CONF        Path to config file (default: conf/statica.conf)
  --mode MODE        Mode [(d)aemon|(s)ingle] (default single)
  --report           Always show a report in single mode
  --project PROJECT  Only check specified project (must be defined in conf)
  --start START      Start from specific tag/commit (-[digit] to count back
                     from last tag)
  --reload           Refresh project data from git
  --check CHECK      Only run specific checks, overrides config (ie:
                     csrf,danger)
  --errors           Show parsing errors
```