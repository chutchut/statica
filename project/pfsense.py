from phply.phpast import *
from project import StaticaProject

class pfSenseProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['cache', 'images'])
        self.inputs.extend([('func', 'getVal', None)])
        self.fMatch = ['www']
        self.cMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers']