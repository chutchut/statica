from phply.phpast import *
from project import StaticaProject


class TestProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.inputs.extend([('func', 'get', '*input'), ('func', 'get', '*get'), ('func', 'get', '*post'), ('func', 'getState', None), ('func', 'getUserState', None), ('func', '*FromRequest', None)])
        self.fMatch = ['test', 'Test', 'Class']
        self.cMatch = ['Class', 'test']
        self.valFuncs.extend([('*db*', 'quote*', True), ('*db*', 'q', True), ('*db*', 'escape', True), ('*query*', 'cast*', True), ('ArrayHelper', 'toInteger', False), 'dbQuote'])
        self.checkConf.extend([
            {'access': {
                'filter': self.fMatch,
                'check_parent': False,
                'context': 'outer.nonfunc',
                'name_pattern': None,
                'modifiers': None
            }},
            {'csrf': {
                'filter': self.cMatch,
                'check_parent': True,
                'context': 'inner',
                'name_pattern': '^[a-zA-Z0-9]+[a-zA-Z0-9_]*$',
                'modifiers': ['public']
            }},
            {'xss': {
                'filter': self.cMatch,
                'check_parent': False,
                'context': 'inner',
                'name_pattern': None,
                'modifiers': None
            }},
            {'sqli': {
                'filter': self.cMatch,
                'check_parent': False,
                'context': None,
                'name_pattern': None,
                'modifiers': None
            }},
        ])
        
    def check_access(self, path, cls, fnodes, local=None):
        fn = self.getFunctions(fnodes, 'defined')
        foundJExec = [] 
        for f in fn:
            if hasattr(f, 'params') and f.name == 'defined':                 
                for p in self.getFuncParams(f):
                    if p.node == '_TEST':
                        foundJExec.append(True)
        if True not in foundJExec:
            self.addResult(('n/a', cls, path))
                
    def check_csrf(self, path, cls, fnodes, local=None):
        foundCSRFChk = False
        if self.hasFunction(fnodes, 'antiCSRF'):
            foundCSRFChk = True
        if foundCSRFChk is False:
            self.addResult((fnodes.name, cls, path))
    
    def check_xss(self, path, cls, fnodes, local=None):
        ns = self.getUserInputs(fnodes, self.inputs)
        report = False
        for n in ns:
            # Check for global
            if self.isObject(n, Variable):
                report = True
        if report:
            self.addResult((fnodes.name, cls, path))
        
    def check_sqli(self, path, cls, fnodes, local=None):
        dbFuncs = []
        dbFuncs.extend(self.getFunctions(fnodes, 'select'))
        dbFuncs.extend(self.getFunctions(fnodes, 'update'))
        dbFuncs.extend(self.getFunctions(fnodes, 'delete'))
        dbFuncs.extend(self.getFunctions(fnodes, 'set'))
        dbFuncs.extend(self.getFunctions(fnodes, 'from'))
        dbFuncs.extend(self.getFunctions(fnodes, 'where'))
        for dbFunc in dbFuncs:
            bOp = self.getAllNodes(BinaryOp, dbFunc)
            # Only concat ops
            bOpConcat = [op for op in bOp if op.op == '.']
            if len(bOpConcat) > 0:
                vars = self.getVariableInputs(bOpConcat)
                for var in vars:
                    varState = self.getVarState(var)
                    if varState is not None:
                        if varState.wasCast() is False and varState.wasValidated() is False:
                                self.addResult((fnodes.name, cls, path)) 
