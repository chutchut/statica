from phply.phpast import *
from project import StaticaProject

class phpBBProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['docs', 'build', 'git-tools', 'tests', 'travis'])
        self.inputs.extend([('func', 'getVal', None)])
        self.fMatch = ['phpBB']
        self.cMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers']