from phply.phpast import *
from project import StaticaProject

class MagentoProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['Test', 'dev', 'vendor'])
        self.inputs.extend([('func', 'getParams', 'getRequest'), ('func', 'getParam', 'getRequest'), ('func', 'getPost', 'getRequest'), ('func', 'getGet', 'getRequest')])
        self.fMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers', 'Model', 'Block', 'Helper', 'app']
        self.cMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers']
        
    def check_csrf(self):
        print('Checking for CSRF issues')
        
        allFnc = self.getParsedFiles(self.parsed, self.cMatch)
        r = []
        for c in allFnc:
            clsLst = c.inner
            fpath = c.path
            for c in clsLst:
                for f in c[1]:
                    cls = c[0]
                    inp = []
                    foundInput = False
                    foundCSRFChk = False
                    if hasattr(f, 'name') and f.name.startswith('_'):
                        continue
                    if self.isParseable(f) is False:
                        continue
                    inp.extend(self.getUserInputs(f, self.inputs))
                    if len(inp) > 0:
                        foundInput = True
                    else:
                        continue
                    if self.getFunction(f, 'getParam', 'key') is not None or self.getFunction(f, 'getParam', 'form_key') is not None:
                        foundCSRFChk = True
                    if self.hasFunction(f, 'validate', '_formKeyValidator') or self.hasFunction(f, '_validateFormKey'):
                        foundCSRFChk = True
                    if foundInput is True and foundCSRFChk is False:
                        r.append((f.name, cls, fpath))
        self.res['csrf'] = r
        print('Found %d results from CSRF checks' % (len(r)))  
        
    def check_xss(self):
        print('Checking for XSS issues')

        filter = self.cMatch
        filter.extend(['app'])
        allFnc = self.getParsedFiles(self.parsed, filter)
        r = []
        for c in allFnc:
            clsLst = c.inner
            fpath = c.path
            for c in clsLst:
                for f in c[1]:
                    cls = c[0]
                    if self.isParseable(f) is False:
                        continue
                    ns = self.getUserInputs(f, self.inputs)
                    if len(ns) > 0 or len(self.getFuncParams(f)) > 0:
                        # Check for missing escapeHtml
                        if self.hasFunction(f, 'escapeHtml') is False and self.hasFunction(f, 'escapeHtml', None, False) is False:
                            res = (f.name, cls, fpath)
                            if res not in r:
                                r.append(res)
                    for n in ns:
                        # Check for global
                        if self.isObject(n, Variable):
                            res = (f.name, cls, fpath)
                            if res not in r:
                                r.append(res)
        self.res['xss'] = r
        print('Found %d results from XSS checks' % (len(r)))
