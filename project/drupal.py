from phply.phpast import *
from project import StaticaProject

class DrupalProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.inputs.extend([('func', 'drupal_get_form', None), ('func', 'drupal_form_submit', None)])
        self.fMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers', 'Form', 'Entity', 'Plugin', 'src', 'core', 'sites', 'scripts', 'includes', 'modules', 'themes']
        self.cMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers', 'Form', 'Entity', 'Plugin', 'src']
                
    def check_csrf(self):
        print('Checking for CSRF issues')
        
        allFnc = self.getParsedFiles(self.parsed, self.cMatch)
        r = []
        for c in allFnc:
            clsLst = c.inner
            fpath = c.path
            for c in clsLst:
                for f in c[1]:
                    cls = c[0]
                    inp = []
                    foundCSRFChk = False
                    if hasattr(f, 'name') and f.name.startswith('_'):
                        continue
                    if self.isParseable(f) is False:
                        continue
                    inp.extend(self.getUserInputs(f, self.inputs))
                    if len(inp) == 0 and len(self.getFuncParams(f)) == 0:
                        return
                    if self.hasFunction(f, 'drupal_check_token') or self.hasFunction(f, 'drupal_valid_token'):
                        foundCSRFChk = True
                    if foundCSRFChk is False:
                        r.append((f.name, cls, fpath))
        self.res['csrf'] = r
        print('Found %d results from CSRF checks' % (len(r)))   
        
    def check_xss(self):
        print('Checking for XSS issues')

        filter = self.cMatch
        filter.extend(['core'])
        allFnc = self.getParsedFiles(self.parsed, filter)
        r = []
        for c in allFnc:
            clsLst = c.inner
            fpath = c.path
            for c in clsLst:
                for f in c[1]:
                    cls = c[0]
                    if self.isParseable(f) is False:
                        continue
                    ns = self.getUserInputs(f, self.inputs)
                    if len(ns) > 0 or len(self.getFuncParams(f)) > 0:
                        # Check for missing filter_xss
                        if self.hasFunction(f, 'filter_xss') is False:
                            res = (f.name, cls, fpath)
                            if res not in r:
                                r.append(res)
                    for n in ns:
                        # Check for global
                        if self.isObject(n, Variable):
                            res = (f.name, cls, fpath)
                            if res not in r:
                                r.append(res)
        self.res['xss'] = r
        print('Found %d results from XSS checks' % (len(r)))
