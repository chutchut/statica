from phply.phpast import *
from project import StaticaProject

class MediaWikiProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['tests', 'docs'])
        self.inputs.extend([('func', 'getVal', None)])
        self.fMatch = ['includes', 'maintenance', 'extensions', 'skins']
        self.cMatch = ['Controller', 'Controllers', 'Controller.php', 'controllers']