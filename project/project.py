import sys, os, traceback, time, re, pickle
import base64, time, hashlib, random
import inspect

from multiprocessing.pool import ThreadPool

from phply import phplex
from phply.phpast import *


class StaticaState():
    def __init__(self, sParsed, projInstance):
        self.staticaParsed = sParsed
        self.project = projInstance
        self.varState = {}
        self.__buildVariableState()
        
    def getHashes(self):
        return self.varState.keys()
    
    def getHashesInScope(self, funcName, className, path, scope='func'):
        inScope = []
        pattern = self.__getScopeFilterPattern(funcName, className, path, scope)
        hashes = self.getHashes()
        for hash in hashes:
            if hash.endswith(pattern):
                inScope.append(hash)
        return inScope
    
    def getVarsInScope(self, funcName, className, path, scope='func'):
        hashes = self.getHashesInScope(funcName, className, path, scope)
        vars = []
        for hash in hashes:
            vars.append(self.varState[hash])
        return vars
    
    def getVarsInScopeFromState(self, state, scope='func'):
        return self.getVarsInScope(state.funcName, state.className, state.path, scope)
    
    def getHashesInScopeFromState(self, state, scope='func'):
        return self.getHashesInScope(state.funcName, state.className, state.path, scope)
    
    def __addAll(self, target, nodes):
        for node in nodes:
            if node not in target:
                target.append(node)
        return target
    
    def getAll(self, varState, type='from', mode='from', scope='func'):
        all = []
        if type == 'to':
            self.__addAll(all, varState.assignedTo)
        if type == 'from':
            self.__addAll(all, varState.assignedFrom)
        if type == 'if':
            self.__addAll(all, varState.ifs)
        if type == 'func':
            self.__addAll(all, varState.funcs)
        if type == 'op':
            self.__addAll(all, varState.ops)
        if type in ('from', 'to'):
            mode = type
        varsInScope = self.getVarsInScope(varState.funcName, varState.className, varState.path, scope)
        hasMore = True
        limit = 100
        count = 0
        while hasMore:
            tmp = []
            count += 1
            for var in varsInScope:
                if self.project.isObject(varState, StaticaVariableState) is False:
                    continue
                if varState.wasAssigned(var, mode) or varState.wasReferenced(var):
                    if var not in tmp and var not in all:
                        tmp.append(var)
                    if type == 'to':
                        self.__addAll(all, var.assignedTo)
                    if type == 'from':
                        self.__addAll(all, var.assignedFrom)
                    if type == 'if':
                        self.__addAll(all, var.ifs)
                    if type == 'func':
                        self.__addAll(all, var.funcs)
                    if type == 'op':
                        self.__addAll(all, var.ops)    
            if len(tmp) == 0 or count >= limit:
                hasMore = False
            else:
                varState = tmp[random.choice(range(0, len(tmp)))]
        return all
            
    def __getScopeFilterPattern(self, funcName, className, path, scope):
        if scope == 'func':
            return '::%s::%s::%s' % (funcName, className, path)
        elif scope == 'class':
            return '::%s::%s' % (className, path)
        elif scope == 'path': 
            return '::%s' % (path)
        else:
            return None
        
    def getVarKey(self, var):
        key = None
        if self.project.isObject(var, Variable):
            key = '%s' % (var.name)
        elif self.project.isObject(var, ArrayOffset) and hasattr(var.node, 'name'):
            key = '%s[\'%s\']' % (var.node.name, var.expr)
        elif self.project.isObject(var, ObjectProperty) and hasattr(var.node, 'name'):
            key = '%s->%s' % (var.node.name, var.name)
        elif self.project.isObject(var, ArrayOffset) and self.project.isObject(var.node, ObjectProperty) and hasattr(var.node.node, 'name'):
            key = '%s->%s[\'%s\']' % (var.node.node.name, var.node.name, var.expr)
        elif self.project.isObject(var, ClassConstant):
            key = 'const:%s(%s)' % (var.name.upper(), var.initial)
        elif self.project.isObject(var, ClassVariable):
            key = '%s(%s)' % (var.name, var.initial)
        elif self.project.isObject(var, StaticVariable):
            key = '%s::(%s)' % (var.name, var.initial)
        elif self.project.isObject(var, StaticProperty):
            key = '%s:>(%s)' % (var.name, var.name)
        elif self.project.isObject(var, New):
            key = 'new %s()' % (var.name)
        return key
    
    def getState(self, var, function='', cls='', path=''):
        if self.project.isObject(var, StaticaVariableState):
            if var.hash in self.varState.keys():
                return self.varState[var.hash]
        elif var in self.varState.keys():
            return self.varState[var]
        elif self.project.isObject(var, StaticaVariableState) is False:
            # First try finding it
            hash = self.__findStateHash(var, function, cls, path)
            if hash is not None:
                return self.varState[hash]
        return None

    def getStateByKey(self, key):
        for h, item in self.varState.items():
            if item.key == key:
                return item
        return None

    def stateExists(self, var):
        state = self.getState(var)
        if state:
            return True
        else:
            return False
        
    def __findStateHash(self, var, function, cls, path):
        if type(var) not in self.project.validVarTypes:
            var = Variable(str(var))
        hash = self.getStateHash(StaticaVariableState(self, var, function, cls, path))
        if hash in self.varState.keys() and self.varState[hash] is not None:
            return hash
        else:
            return None     
    
    def getStateHash(self, state):
        if self.project.isObject(state, StaticaVariableState):
            return '%s::%s::%s::%s' % (state.key, state.funcName, state.className, state.path)
        else:
            return None
    
    def __buildVariableState(self):
        # Only build the state for specififc nodes
        if self.staticaParsed.stateNodes is not None:
            print('Building limited variable state for: %s' % (self.staticaParsed.path))
            for nodeEl in self.staticaParsed.stateNodes:
                clsName = None
                for cls in self.staticaParsed.getInnerClasses():
                    # Get class name if in class scope
                    if self.project.inScopeOf(nodeEl, cls):
                        clsName = cls.name
                        break
                for nState in self.__getAssignments(nodeEl, clsName, self.staticaParsed.path):
                    self.addState(nState)
                vars = self.__getVars(nodeEl, clsName, self.staticaParsed.path)
                for var in vars:
                    self.__getIfs(nodeEl, var)
                    self.__getOps(nodeEl, var)
                    self.__getCalledFuncs(nodeEl, var)
                    self.addState(var)
            return
        print('Building full variable state for: %s' % (self.staticaParsed.path))
        for cls in self.staticaParsed.inner:
            for element in cls[1]:
                for iState in self.__getAssignments(element, cls[0], self.staticaParsed.path):
                    self.addState(iState)
                vars = self.__getVars(element, cls[0], self.staticaParsed.path)
                for var in vars:
                    self.__getIfs(element, var)
                    self.__getOps(element, var)
                    self.__getCalledFuncs(element, var)
                    self.addState(var)
        for outerEl in self.staticaParsed.outer:
            for oState in self.__getAssignments(outerEl, None, self.staticaParsed.path):
                self.addState(oState)
            vars = self.__getVars(outerEl, None, self.staticaParsed.path)
            for var in vars:
                self.__getIfs(outerEl, var)
                self.__getOps(outerEl, var)
                self.__getCalledFuncs(outerEl, var)
                self.addState(var)
        print('[%s]: State cache has %d keys' % (self.staticaParsed.path, self.getNumVars()))

    def getNumVars(self):
        return len(self.varState.keys())

    def addState(self, state):
        if state.key == '$this':
            return
        if state.hash not in self.varState.keys():
            self.varState[state.hash] = state
        else:
            self.varState[state.hash].update(state)
                
    def __getAssignments(self, element, cls, path):
        assignList = []
        assignments = self.project.getAssignments(None, element, all=True)
        for assignment in assignments:
            toBeAssigned = self.__getAssignment(assignment, element, cls, path)
            for assign in toBeAssigned:
                if assign not in assignList:
                    assignList.append(assign)
                else:
                    for assigned in assignList:
                        if assigned == assign:
                            for to in assign.assignedTo:
                                if to not in assign.assignedTo:
                                    assigned.assignedTo.append(to)
                            for frm in assign.assignedFrom:
                                if frm not in assign.assignedFrom:
                                    assigned.assignedFrom.append(frm)
        return assignList
            
    def __getAssignment(self, assignment, element, cls, path):
        states = []
        for nodeType in ['node', 'expr']:
            node = getattr(assignment, nodeType)
            state = self.__makeStateVar(element, cls, node, path)
            if state is not None and state not in states:
                if nodeType == 'node':
                    otherNode = getattr(assignment, 'expr')
                    assignList = state.assignedFrom
                else:
                    otherNode = getattr(assignment, 'node')
                    assignList = state.assignedTo
                if otherNode not in assignList:
                    assignList.append(otherNode)
                states.append(state)
        return states
    
    def __makeStateVar(self, function, cls, node, path):
        params = []
        if type(node) not in self.project.validVarTypes:
            return None
        if function is not None and hasattr(function, 'name') and type(function) in self.project.validFuncDefTypes:
            funcName = function.name
            if hasattr(function, 'params') and len(params) == 0:
                for param in self.project.getAllNodes(FormalParameter, function.params):
                    params.append(param.name)
        else:
            funcName = None
            
        if cls is not None:
            clsName = cls
        else:
            clsName = None

        # Now check for func param
        if hasattr(node, 'name') and node.name in params:
            isFuncParam = True
        else:
            isFuncParam = False
            
        state = StaticaVariableState(self, node, funcName, clsName, path, isFuncParam)
        return state
    
    def __getVars(self, function, clsName, path):
        vars = []
        states = []
        for varType in self.project.validVarTypes:
            for var in self.project.getAllNodes(varType, function):
                if var not in vars:
                    vars.append(var)
        for varBlock in vars:
            state = self.__makeStateVar(function, clsName, varBlock, path)
            if state:
                states.append(state)
        return states

    def __getIfs(self, allElements, varState):
        ifs = []
        ifs.extend(self.project.getAllNodes(If, allElements))
        ifs.extend(self.project.getAllNodes(ElseIf, allElements))
        for ifBlock in ifs:
            if self.project.inScopeOf(varState.varRef, ifBlock.expr):
                if ifBlock not in varState.ifs:
                    varState.ifs.append(ifBlock)

    def __getOps(self, allElements, varState):
        ops = []
        ops.extend(self.project.getAllNodes(BinaryOp, allElements))
        ops.extend(self.project.getAllNodes(AssignOp, allElements))
        ops.extend(self.project.getAllNodes(UnaryOp, allElements))
        ops.extend(self.project.getAllNodes(TernaryOp, allElements))
        for opBlock in ops:
            if self.project.inScopeOf(varState.varRef, opBlock):
                if opBlock not in varState.ops:
                    varState.ops.append(opBlock)

    def __getCalledFuncs(self,  allElements, varState):
        funcs = []
        for funcType in self.project.validFuncTypes:
            funcs.extend(self.project.getAllNodes(funcType, allElements))
        for funcBlock in funcs:
            if self.project.inScopeOf(varState.varRef, funcBlock):
                if funcBlock not in varState.funcs:
                    varState.funcs.append(funcBlock)
        
class StaticaVariableState():
    def __init__(self, state, ref, funcName, className, path, isFuncParam=False):
        self.state = state
        self.varRef = ref
        self.funcName = funcName
        self.funcParam = isFuncParam
        self.className = className
        self.path = path
        self.key = self.state.getVarKey(ref)
        self.hash = self.state.getStateHash(self)
        self.assignedTo = []
        self.assignedFrom = []
        self.ifs = []
        self.funcs = []
        self.ops = []

    def __eq__(self, other):
        if type(self) is not type(other):
            return False
        else:
            return self.hash == other.hash
    
    def __ne__(self, other):
        if type(self) is not type(other):
            return False
        else:
            return self.hash != other.hash

    def __hash__(self):
        return hash((self.key, self.funcName, self.className, self.path))
    
    def __repr__(self):
        return '<[%s]>' % (self.hash)
    
    def update(self, state):
        attrs = ('assignedTo', 'assignedFrom', 'ifs', 'funcs', 'ops')
        for attr in attrs:
            for el in getattr(state, attr):
                toList = getattr(self, attr)
                if el not in toList:
                    toList.append(el)
        
    def wasValidated(self):
        # Get all functions that reference this var
        funcFrm = self.state.getAll(self, type='func', mode='from')
        frm = self.state.getAll(self, type='from', mode='from')
        # Append ops to the list
        frm.extend(self.state.getAll(self, type='op', mode='from'))
        # Add the ref for this var itself
        frm.append(self.varRef)
        for valFunc in self.state.project.valFuncs:
            if isinstance(valFunc, tuple):
                valFuncs = self.state.project.getFunctions(funcFrm, valFunc[1], valFunc[0], valFunc[2])
            else:
                valFuncs = self.state.project.getFunctions(funcFrm, valFunc)
            for fromEl in frm:
                if self.state.project.inScopeOf(fromEl, valFuncs):
                    return True
        return False

    def wasReferenced(self, varState):
        return self.state.project.inScopeOf(self.varRef, varState.varRef)
        
    def wasAssigned(self, varState, mode='to'):
        if mode == 'to':
            assignList = self.assignedTo
        elif mode == 'from':
            assignList = self.assignedFrom
        else:
            assignList = self.assignedTo
        return self.state.project.inScopeOf(varState.varRef, assignList)
                            
    def wasCast(self, type=None):
        frm = self.state.getAll(self, type='from', mode='from')
        frm.extend(self.state.getAll(self, type='op', mode='from'))
        casts = self.state.project.getAllNodes(Cast, frm)
        if type is None:
            return len(casts) > 0
        else:
            foundType = False
            for cast in casts:
                if cast.type == type:
                    foundType = True
                    break
            return foundType
                    
class StaticaParsed():
    def __init__(self, path, inner, project, outer=[]):
        self.path = path
        self.inner = inner
        self.outer = outer
        self.project = project
        self.stateNodes = None
        self.state = None
        
    def getState(self, nodes=None):
        if self.state is None:
            self.stateNodes = nodes
            # Rebuild if state nodes change
            self.state = StaticaState(self, self.project)
        return self.state
    
    def getInnerClasses(self):
        clsNodes = []
        for cls in self.inner:
            clsNodes.extend(cls[1])
        return clsNodes

    def getAll(self):
        # Second element of tuple is nodes
        classes = self.inner
        all = []
        for c in classes:
            nodes = c[1]
            all.extend(nodes)
        all.extend(self.outer)
        return all
    
class StaticaParser():
    def __init__(self, parser, path):
        self.path = path
        self.errors = []
        self.errCount = 0
        self.totalLines = 0
        self.removedLines = 0
        self.parseableLines = []
        self.rawInput = None
        self.output = None
        self.parser = parser
        self.init()
        
    def init(self):
        try:
            # Read raw file input
            fr = open(self.path)
            inp = fr.read()
            self.rawInput = inp
            self.totalLines = len(inp.splitlines())
        except Exception as e:
            print('Exception reading file: %s' % (self.path))
            
    def p_error(self, t):
        if not t:
            #print("End of File!")
            return
        start = t.lineno
        cur = start
        # Read ahead to get back to a good state..
        #print('Parser error on line: %d, skipping ahead..' % (start))
        while True:
            tok = self.parser.token() # Get the next token
            if tok is not None:
                cur = tok.lineno
            if not tok or tok.type == 'RBRACE':
                break
        # Keep a record of the original error
        err = SyntaxError('Invalid syntax in file: %s (%s)' % (self.path, t.type), (self.path, t.lineno, None, t.value))
        self.errors.append((str(err), start, cur))
        #print('Restarting parser on line: %d' % (cur))
        self.parser.restart()
        
    def parse(self, input=None):
        if input is None:
            input = self.rawInput
        else:
            if len(input) == 0:
                return []
        lxr = phplex.lexer.clone()
        # Set custom error handler so it doesnt bail on SyntaxError
        try:
            self.parser.errorfunc = self.p_error
            output = self.parser.parse(input, lexer=lxr)
            resolve_magic_constants(output)
        except Exception as e:
            #print('Exception in error recovery: %s' % (e))
            self.errors.append((e, 0, self.totalLines))
            output = []
        # Done check for errors
        if len(self.errors) > 0:
            # Remove troublesome lines, one error at a time
            inArr = input.splitlines()
            # Get error start and end lines
            e = self.errors[0]
            if isinstance(e, tuple):
                start = e[1]
                end = e[2]
                arrSlice1 = inArr[:(start - 1)]
                arrSlice2 = inArr[end:] 
                inArr = arrSlice1 + arrSlice2
                input = '\n'.join(inArr)
                errTmp = self.errors
                self.errors = []
                # Stop at 10 attempts, its probably screwed..
                if self.errCount < 10:
                    self.errCount += 1
                    # Recurse
                    output = self.parse(input)
                    # Finished recursing, reset original errors
                    self.errors = errTmp
                self.parseableLines = input.splitlines()
                self.removedLines = (len(self.rawInput.splitlines()) - len(self.parseableLines))
                self.output = output
        else:
            self.parseableLines = input.splitlines()
            self.removedLines = (len(self.rawInput.splitlines()) - len(self.parseableLines))
            self.output = output
        return output
            
    def getSkipped(self):
        skip = 0
        for e in self.errors:
            if isinstance(e, tuple):
                skip += (e[2] - e[1])
        return skip
        
    def getErrors(self):
        return self.errors
        
class StaticaProject():
    def __init__(self, name, version, check, conf, inst, db, obj):
        self.name = name
        self.version = version
        self.toCheck = check.split(',')
        self.inputs = [
            (Variable, '$_COOKIE', None), (Variable, '$_REQUEST', None), (Variable, '$_GET', None), (Variable, '$_POST', None),
            (ArrayOffset, 'Variable:$_SERVER', 'HTTP_*'),  # Assume header vars share the same prefix
        ]
        self.files = []
        self.parsed = ()
        self.errors = []
        self.excludedPaths = []
        self.totalLines = 0.0
        self.skipCount = 0.0
        self.parseCount = 0.0
        self.totalFileCount = 0.0
        self.fileModCount = 0.0
        self.res = {}
        self.context = None
        self.conf = conf
        self.statica = inst
        if self.statica.useCheckThreads:
            self.tPool = self.initThreadPool()
        self.fMatch = ['.']
        self.db = db
        if not conf['standalone']:
            self.path = '%s/%s' % (inst.getGlobalConf('git_root_path'), name.lower())
        else:
            self.path = conf['clone_url']
        self.dangerFuncs = ['exec', 'system', 'shell_exec', 'popen', 'passthru', 'pcntl_exec', 'unserialize', 'unlink', 'rmdir', 'fwrite', 'fopen', 'file_put_contents', 'file_get_contents', 'eval', 'call_user_func*']
        self.valFuncs = ['is_numeric']
        self.validFuncDefTypes = [Method, Function]
        self.validFuncTypes = [MethodCall, StaticMethodCall, FunctionCall, Silence]
        self.validVarTypes = [Variable, Array, ArrayOffset, ArrayElement, ObjectProperty, ClassConstant, ClassVariable, StaticVariable, StaticProperty, New]
        self.validAssignTypes = [Assignment, ListAssignment]
        self.excludedTypes = [InlineHTML, Class, Interface]
        self.validObj = obj
        self.checkConf = [
            {'danger': {'filter': ['.'], 'check_parent': 0}},
            {'includes': {'filter': ['.'], 'check_parent': 0}},
        ]
            
    def initThreadPool(self):
        return ThreadPool(processes=self.statica.numCheckThreads)
    
    def addResult(self, res):
        caller = sys._getframe().f_back.f_code.co_name
        check = caller.replace('check_', '')
        if res not in self.res[check]:
            self.res[check].append(res)
            
    def getResult(self, check):
        if check in self.res:
            return self.res[check]
        else:
            return []

    def getVarState(self, var, parsed=None, statenodes=None):
        args = inspect.getargvalues(sys._getframe().f_back)
        if 'cls' not in args[3].keys() or 'path' not in args[3].keys() or 'fnodes' not in args[3].keys():
            print('Couldnt find caller args (cls, path, fnodes), are you calling from the right context?')
            return None
        else:
            cls = args[3]['cls']
            path = args[3]['path']
            fnodes = args[3]['fnodes']
            if type(fnodes) in self.validFuncDefTypes and hasattr(fnodes, 'name'):
                funcName = fnodes.name
            else:
                funcName = None
            if parsed is None:
                parsed = self.getParsedFileFromPath(self.parsed, path)
            if parsed is None:
                return None
            elif statenodes is not None:
                return parsed.getState(nodes=statenodes).getState(var, funcName, cls, path)
            else:
                return parsed.getState().getState(var, funcName, cls, path)
    
    def __getFilter(self, check):
        cConf = self.__getCheckConf(check)
        if cConf is not None and 'filter' in cConf:
            return cConf['filter']
        else:
            return '.'
            
    def __getCheckConf(self, check):
        for cc in self.checkConf:
            if check in cc:
                return cc[check]
        return None
        
    def __getCheckConfParam(self, check, confVar):
        cConf = self.__getCheckConf(check)
        if cConf is not None and confVar in cConf:
            return cConf[confVar]
        else:
            return None
    
    def __checkWrapper(self, mName):
        check = mName.replace('check_', '')
        self.res[check] = []
        filter = self.__getFilter(check)
        checkMethod = getattr(self, mName) 
        parsedFiltered = self.getParsedFiles(self.parsed, filter)
        print('Starting check: %s' % (mName))
        for pf in parsedFiltered:
            # Get locally (from the same file/path) scoped tokens (inner + outer)
            localTokens = pf.getAll()
            # Inner (class context, functions)
            self.context = 'inner.func'
            if self.__checkContext(check, self.context):
                for cls in pf.inner:
                    clsName = cls[0]
                    for innerFunc in cls[1]:
                        if type(innerFunc) not in self.validFuncDefTypes:
                            continue
                        # Check function name
                        if self.__getCheckConfParam(check, 'name_pattern') is not None and re.search(self.__getCheckConfParam(check, 'name_pattern'), innerFunc.name) is None:
                            continue
                        # Check function modifiers
                        if self.__getCheckConfParam(check, 'modifiers') is not None and self.hasFuncModifiers(self.__getCheckConfParam(check, 'modifiers'), innerFunc) is False:
                            continue
                        # Check for parent calls and recurse through
                        if self.__getCheckConfParam(check, 'check_parent') == 1 or self.__getCheckConfParam(check, 'check_parent') is True:
                            self.checkParentFunctions(checkMethod, pf.path, clsName, innerFunc)
                            hier = self.getParentHierarchy(innerFunc, clsName)
                            if len(hier) == 0 or self.hasResultsFor(check, hier) is True:
                                checkMethod(pf.path, clsName, innerFunc, localTokens)
                        else:
                            checkMethod(pf.path, clsName, innerFunc, localTokens)
            # Outer (no class context, functions)
            self.context = 'outer.func'
            if self.__checkContext(check, self.context):
                for el in pf.outer:
                    if type(el) in self.validFuncDefTypes:
                        # Pass in function blocks to the check
                        checkMethod(pf.path, None, el, localTokens)
            # Outer (no class context, outside functions)
            self.context = 'outer.nonfunc'
            if self.__checkContext(check, self.context):
                nonFuncNodes = []
                for el in pf.outer:
                    if type(el) not in self.validFuncDefTypes:
                        nonFuncNodes.append(el)
                # And the rest of the outer nodes outside of functions
                checkMethod(pf.path, None, nonFuncNodes, localTokens)
        print('Found %d results from [%s]' % (len(self.res[check]), mName)) 
        
    def __checkContext(self, check, curContext):
        checkContext = self.__getCheckConfParam(check, 'context')
        if checkContext is None:
            return True
        elif '.' in checkContext:
            if curContext == checkContext:
                return True
            else:
                return False
        elif curContext.startswith(checkContext):
            return True
        else:
            return False
        
    def hasResultsFor(self, check, list): 
        res = self.getResult(check)
        for l in list:
            for r in res:
                if l[0] == r[0] and l[1] == r[1] and l[2] == r[2]:
                    return True
        return False
        
    def checkParentFunctions(self, checkMethod, path, cls, inner): 
        if self.hasParentFunctionCall(inner) is False:
            return
        parentClassName = self.getClassExtends(self.parsed, cls)
        parentClassTuple = self.getClass(self.parsed, parentClassName)
        if parentClassTuple is None:
            print('Cant find class [%s] in parsed files, maybe an external lib? Try extending the fMatch variable' % (parentClassName))
            return
        parentClassObj = parentClassTuple[0]
        parentClassPath = parentClassTuple[1]
        parentCalls = self.getParentFunctionCalls(inner)
        for parentCall in parentCalls:
            print('Checking parent call (parent::%s()) from class (%s) in file: %s' % (parentCall.name, cls, path))
            pcNodes = self.getFunctionDefs(parentClassObj[1], parentCall.name)
            if pcNodes is not None:
                checkMethod(parentClassPath, parentClassObj[0], pcNodes)
                # Recurse to find other parent calls in the hierarchy
                self.checkParentFunctions(checkMethod, parentClassPath, parentClassObj[0], pcNodes)
        
    def check(self):
        print('Starting run of %s (%s)' % (self.name, self.version))
        print('Checks defined for %s: %s' % (self.name, self.toCheck))
        # Get db ids
        pRec = self.db.getProject(self.name)
        vRec = self.db.getVersion(self.version)
        pID = pRec[0]
        vID = vRec[0]
        # Proj/version lookup
        if self.db.getProjectVersion(pID, vID) is None:
            self.db.query('INSERT INTO project_to_version (proj_id, ver_id) VALUES (%d, %d)' % (int(pID), int(vID)))
        self.files = self.getFiles(self.path, self.fMatch)
        #DEBUG: Set specific file
        #self.files = ['/opt/lampp/htdocs/statica/joomla/components/com_finder/helpers/html/filter.php']
        self.totalFileCount = len(self.files)
        print('Parsing project source.. (%d files)' % (self.totalFileCount))
        # Cache class names and paths on first version
        if self.conf['check_freq'] not in self.conf.keys() or self.version == self.conf[self.conf['check_freq']][0]:
            self.statica.classCache[self.name] = {}
            print('Building class cache..')
        # Start timer
        allCheckStartTime = time.time()
        self.statica.dispatcher.dispatch('parse_start', (self.name, self.version))
        if self.statica.useCheckThreads:
            self.parsed = self.multiParse(self.files)
        else:
            self.parsed = self.parse(self.files)
        self.getSupplementalParsedFiles()
        self.statica.dispatcher.dispatch('parse_finish', (self.name, self.version))
        endTime = time.time()
        elapsed = float('%.2f' % (endTime - allCheckStartTime))
        parseTime = elapsed
        self.parseCount = (self.totalLines - self.skipCount)
        if self.totalLines > 0:
            parsePct = (self.parseCount / self.totalLines) * 100
            if elapsed > 0:
                rate = (self.parseCount / elapsed)
            else:
                rate = self.parseCount
            print('Parsed %d of %d lines (%.2f%%) in %s seconds (%.2f lines p/s avg)' % (self.parseCount, self.totalLines, float(parsePct), elapsed, float(rate)))
            print('Had to skip %d lines because of parsing errors' % self.skipCount)
        for c in self.toCheck:
            try:
                mName = 'check_%s' % (c)
                tRec = self.db.getType(c)
                tID = tRec[0]
                # Start timer
                checkStartTime = time.time()
                self.statica.dispatcher.dispatch('check_start', (self.name, self.version, mName))
                self.__checkWrapper(mName)
                self.statica.dispatcher.dispatch('check_finish', (self.name, self.version, mName))
                endTime = time.time()
                elapsed = float('%.2f' % (endTime - checkStartTime))
                # Save check metadata
                q = 'INSERT INTO check_metadata (proj_id, ver_id, type_id, duration_sec, utime) VALUES (%d, %d, %d, %f, %d)' % (pID, vID, tID, elapsed, int(time.time()))
                self.db.query(q)
                print('Completed check (%s) in %s seconds' % (mName, elapsed))
            except Exception as e:
                print('Exception calling method: %s (%s)' % (mName, e))
                traceback.print_exc()
        # Report parsing errors
        if len(self.errors) > 0:
            print('%d errors occurred while parsing' % (len(self.errors)))
        allCheckEndTime = time.time()
        elapsed = float('%.2f' % (allCheckEndTime - allCheckStartTime))
        elapsedMin = float('%.2f' % (elapsed / 60))
        print('All checks completed in %s seconds (%s min)' % (elapsed, elapsedMin))
        print('')
        # Save to db
        try: 
            errData = base64.b64encode(pickle.dumps(self.errors))
        except Exception as e:
            print('Exception encoding error data: %s' % (e))
            errData = base64.b64encode(str(e))
        q = '''INSERT INTO parse_metadata (proj_id, ver_id, duration_sec, total_lines, parsed_lines, skipped_lines, utime, errors, total_files, file_list) 
                VALUES (%d, %d, %f, %f, %f, %f, %d, '%s', %d, '%s')''' % (pID, vID, parseTime, self.totalLines, self.parseCount, self.skipCount, int(time.time()), errData, len(self.files), ','.join(self.files))
        self.db.query(q)
        # Loop through the data and save
        for k in self.res.keys():
            tRec = self.db.getType(k)
            tID = tRec[0]
            iData = self.res[k]
            for i in iData:
                q = '''INSERT INTO issue (proj_id, ver_id, type_id, func, class, path) VALUES (%d, %d, %d, '%s', '%s', '%s')''' % (pID, vID, tID, i[0], i[1], i[2])
                self.db.query(q)
                
    def getFiles(self, root, filters, ftype='php', force=False):
        paths = []
        dataToStore = []
        hashMap = self.db.getFileHashmapData(self.name)
        if len(hashMap) == 0:
            print('Please wait while the projects file hash table is initially created..')
        for path, subdirs, files in os.walk(root):
            for name in files:
                for f in filters:
                    if (f in path or f in name) and name.endswith('.%s' % (ftype)):
                        fpath = '%s/%s' % (path, name) 
                        if fpath not in paths:
                            # Check in the hashmap table for files that have changed
                            fHash = self.getFileHashFromPath(fpath)
                            dbHash = self.getFileHashFromHashmap(fpath, hashMap)
                            if dbHash is None:
                                # File not in the hashmap yet, add path to list and entry to hashmap
                                dataToStore.append((fpath, fHash, fHash, self.name, True))
                                self.fileModCount += 1
                            else:
                                if dbHash != fHash or force is True:
                                    # File has changed, add to list and update hashmap entry
                                    dataToStore.append((fpath, dbHash, fHash, self.name, False))
                                    self.fileModCount += 1      
        for f in dataToStore:
            if f[0] not in paths:
                if len(self.excludedPaths) > 0:
                    exclude = False
                    fRelPath = f[0].replace(self.statica.getGlobalConf('git_root_path'), '')
                    for ex in self.excludedPaths:
                        if ex in fRelPath:
                            exclude = True
                    if exclude is False:
                        paths.append(f[0]) 
                        self.db.updateFileHashmapData(f[0], f[1], f[2], f[3], f[4])
                else:
                    paths.append(f[0])
                    self.db.updateFileHashmapData(f[0], f[1], f[2], f[3], f[4])
                if f[4] is True:
                    print('New file in version %s, adding to db (%s)' % (self.version, f[0]))
                else:
                    print('File state has changed in version %s, updating in db (%s)' % (self.version, f[0]))
        # Need to add files where results have found previously to correctly compare between versions
        return paths
    
    def getSupplementalParsedFiles(self):
        # Only need to get supplemental files if its not the first version to be checked and check_parent is True, and proj is not standalone
        supNeeded = False
        if not self.conf['standalone'] and self.version != self.conf[self.conf['check_freq']][0]:
            # Get defined checks
            checks = self.conf['check'].split(',')
            for check in checks:
                if self.__getCheckConfParam(check, 'check_parent'):
                    supNeeded = True
                    break
        if supNeeded:
            print('Getting supplemental data..')
            for parsed in self.parsed:
                for cls in parsed.inner: 
                    self.parseParentClasses(cls[3])
                        
    def parseParentClasses(self, parentClass):
        if not parentClass:
            # No extends
            return
        if self.getClass(self.parsed, parentClass):
            # Already parsed
            return
        # Check for class name alias/override in project if original name not found
        if hasattr(self, 'classAlias') and self.classAlias is not None and parentClass in self.classAlias.keys():
            parentClass = self.classAlias[parentClass]
        if parentClass in self.statica.classCache[self.name].keys():
            parentClassPath = self.statica.classCache[self.name][parentClass]
            if parentClassPath not in self.files:
                print('Missing parent class: %s' % (parentClass))
                parentParsed = self.parse([parentClassPath])
                if parentParsed:
                    self.files.append(parentClassPath)
                    self.parsed.append(parentParsed[0])
                    print('Adding supplemental class: %s' % (parentClass))
                    for cls in parentParsed[0].inner: 
                        self.parseParentClasses(cls[3])
    
    def getFileHashFromHashmap(self, path, hData):
        hash = None
        for r in hData:
            if r[4] == path:
                hash = r[2]
                break
        return hash
    
    def getFileHashFromPath(self, path):
        hash = hashlib.md5()
        with open(path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash.update(chunk)
        return hash.hexdigest()
    
    def getParsedFiles(self, parsed, filters):
        pList = []
        for p in parsed:
            path = p.path
            for f in filters:
                if f in path and p not in pList:
                    pList.append(p)
        return pList
    
    def getParsedFileFromPath(self, parsed, path):
        for pf in parsed:
            if pf.path == path:
                return pf
        return None
    
    def multiParse(self, files):
        # Divide files into lists for each thread
        n = self.statica.numCheckThreads
        print('Splitting work into %d threads..' % (self.statica.numCheckThreads))
        fileSplit = [ files[i::n] for i in xrange(n) ]
        resObjList = []
        resList = []
        self.tPool = self.initThreadPool()
        for fs in fileSplit:
            res = self.tPool.apply_async(self.parse, (fs,))
            resObjList.append(res)
        self.tPool.close()
        self.tPool.join()
        print('All threads finished')
        for res in resObjList:
            result = res.get()
            resList.extend(result)
        return resList
        
    def parse(self, files):
        fnc = []
        for f in files:
            #print('Processing file: %s' % (f))
            sp = StaticaParser(self.statica.getParser(), f)
            # Record total lines before parsing in case of error, counts are preserved for the catch
            self.totalLines += sp.totalLines
            try:
                output = sp.parse()
                self.skipCount += sp.getSkipped()
                self.errors.extend(sp.getErrors())
                res = self.getAllObjects(output, f)
                fnc.append(res)
                #print(output)
            except Exception as e:
                # Add all lines because Exception means we lost the file content
                self.skipCount += sp.totalLines
                self.errors.append((f, traceback.format_exc()))
                if self.statica.showErrors:
                    print('Exception while parsing: %s (%s)' % (e, f))
                    traceback.print_exc()
            finally:
                # Reset the parser for next run
                self.statica.getParser().restart()
        return fnc
        
    def getAllObjects(self, tc, fname): 
        classes = []
        of = []
        for e in tc: 
            if self.isObject(e, Class):
                name = e.name
                nodes = e.nodes
                clsType = e.type
                clsExtends = e.extends
                clsImplements = e.implements
                # Could be more than one class per file
                classes.append((name, nodes, clsType, clsExtends, clsImplements))
                # Build class cache
                self.statica.classCache[self.name][name] = fname
            else:
                # Get outer elements
                of.append(e)
        return StaticaParsed(fname, classes, self, of)
    
    def matchWildcard(self, val, inp):
        # Check for null
        if not val:
            return False
        # Check for exact match
        if val == inp:
            return True
        # Check for invalid values
        if self.isObject(val, str) is False or self.isObject(inp, str) is False or '*' not in val:
            return False
        match = False
        # Get wildcard type
        matchAny = re.match('^\*(.*)\*$', val)
        matchEnd = re.match('^\*(.*)$', val)
        matchStart = re.match('^(.*)\*$', val)
        if matchAny is not None:
            # Match val anywhere in string
            name = matchAny.group(1)
            if name in inp:
                match = True
        if matchAny is None and matchEnd is not None:
            # Match val at end of string
            name = matchEnd.group(1)
            ptn = '^(.*)%s$' % (name)
            if re.match(ptn, inp) is not None:
                match = True
        if matchAny is None and matchStart is not None:
            # Match val at start of string
            name = matchStart.group(1)
            ptn = '^%s(.*)$' % (name)
            if re.match(ptn, inp) is not None:
                match = True
        return match
    
    def inScopeOf(self, node, nodes):
        nodeType = type(node)
        nodesInNode = self.getAllNodes(nodeType, nodes)
        if node in nodesInNode:
            return True
        else:
            return False
        
    def inFunctionScope(self, nodes):
        if type(nodes) in self.validFuncDefTypes:
            return True
        else:
            return False
    
    def hasFuncModifiers(self, mods, func):
        hasMod = False
        if hasattr(func, 'modifiers') is False:
            return False
        for m in mods:
            if m in func.modifiers:
                hasMod = True
                break
        return hasMod
    
    def getAssignments(self, node, nodes, all=False):
        assign = []
        allAssign = self.getAllNodes(Assignment, nodes)
        if all:
            return allAssign
        for assignNode in allAssign:
            if assignNode.node is node or assignNode.expr is node:
                assign.append(assignNode)
        return assign
    
    def getFuncParams(self, func):
        params = []
        if hasattr(func, 'params'):
            params = func.params
        return params
                    
    def getFunctions(self, nodes, name, cont=None, inst=True):
        m = []
        if inst:
            nl = self.getAllNodes(MethodCall, nodes) 
            nl.extend(self.getAllNodes(FunctionCall, nodes))
            for n in nl:
                op = None
                if hasattr(n, 'node'):
                    op = n.node
                if cont is False and op is None:
                    if self.matchWildcard(name, n.name):
                        m.append(n)
                else:
                    if cont is not None and op is not None:
                        # Deal with \ for global namespaces
                        if self.matchWildcard(name, n.name) and hasattr(op, 'name') and (op.name == '\%s' % (cont) or self.matchWildcard(cont, op.name)):
                            m.append(n)
                    else:
                        if self.matchWildcard(name, n.name):
                            m.append(n)
        else:
            nl = self.getAllNodes(StaticMethodCall, nodes)
            for n in nl:
                if cont is False and n.class_ is None:
                    if self.matchWildcard(name, n.name):
                        m.append(n)
                else:
                    if cont is not None:
                        # Deal with \ for global namespaces
                        if self.matchWildcard(name, n.name) and (n.class_ == '\%s' % (cont) or self.matchWildcard(cont, n.class_)):
                            m.append(n)
                    else:
                        if self.matchWildcard(name, n.name): 
                            m.append(n)
        return m
    
    def getFunction(self, nodes, name, param, cont=None, inst=True):
        f = None
        fl = self.getFunctions(nodes, name, cont, inst)
        for fnc in fl:
            fncP = self.getFuncParams(fnc)
            for p in fncP:
                if p.node == param:
                    f = fnc
        return f
    
    def getFunctionDefs(self, nodes, name):
        nl = []
        for fncDef in self.validFuncDefTypes:
            nl.extend(self.getAllNodes(fncDef, nodes))
        for n in nl:
            if self.matchWildcard(name, n.name):
               return n
        return None
    
    def getParentFunctionCalls(self, nodes):
        return self.getFunctions(nodes, '*', 'parent', False)
    
    def getParentHierarchy(self, nodes, cls):
        if self.hasParentFunctionCall(nodes) is False:
            return []
        else:
            hier = []
            parentClassTuple = self.getClass(self.parsed, self.getClassExtends(self.parsed, cls))
            if parentClassTuple is None:
                return []
            parentClass = parentClassTuple[0]
            parentClassPath = parentClassTuple[1]
            parentCalls = self.getParentFunctionCalls(nodes)
            for parentCall in parentCalls:
                toAdd = (parentCall.name, parentClass[0], parentClassPath)
                if toAdd not in hier:
                    hier.append(toAdd)
                pcNodes = self.getFunctionDefs(parentClass[1], parentCall.name)
                if pcNodes is not None:
                    # Recurse to find other parent calls in the hierarchy
                    hier.extend(self.getParentHierarchy(pcNodes, parentClass[0]))
        return hier

    def getSessionVars(self, nodes):
        vars = []
        arrOfs = self.getAllNodes(ArrayOffset, nodes)
        for arrayOffset in arrOfs:
            if self.isObject(arrayOffset.node, Variable) and arrayOffset.node.name == '$_SESSION':
                vars.append(arrayOffset.expr)
        return vars

    def hasSessionVar(self, nodes, name):
        return name in self.getSessionVars(nodes)

    def hasParentFunctionCall(self, nodes):
        return len(self.getParentFunctionCalls(nodes)) > 0
    
    def getClass(self, parsed, name):
        for p in parsed:
            classes = p.inner
            for c in classes:
                if c[0] == name:
                    return (c, p.path)
        # Check for class name alias/override in project if original name not found
        if hasattr(self, 'classAlias') and self.classAlias is not None and name in self.classAlias.keys():
            alias = self.classAlias[name]
            #print('Class [%s] has an alias of: %s' % (name, alias))
            return self.getClass(parsed, alias)
        return None
    
    def getClassType(self, parsed, name):
        clsTuple = self.getClass(parsed, name)
        cls = clsTuple[0]
        if cls is not None:
            return cls[2]
        else:
            return None
        
    def getClassExtends(self, parsed, name):
        clsTuple = self.getClass(parsed, name)
        cls = clsTuple[0]
        if cls is not None:
            return cls[3]
        else:
            return None
        
    def getClassImplements(self, parsed, name):
        clsTuple = self.getClass(parsed, name)
        cls = clsTuple[0]
        if cls is not None:
            return cls[4]
        else:
            return None
    
    def hasFunction(self, nodes, name, cont=None, inst=True):
        return len(self.getFunctions(nodes, name, cont, inst)) > 0
    
    def isParseable(self, obj):
        if type(obj) in self.validObj[0] or self.isObject(obj, list):
            return True
        else:
            return False
        
    def getBinaryIfs(self, nodes, op, opNode, testNode):
        binIfs = []
        nodeIds = ['name', 'expr']
        if op == '==':
            op = '==*'
        if op == '!=':
            op = '!=*'
        ifs = self.getAllNodes(If, nodes)
        for ifNode in ifs:
            # Check the operator
            if self.isObject(ifNode.expr, BinaryOp) and self.matchWildcard(op, ifNode.expr.op):
                # Check the operands
                op1 = False
                op2 = False
                opObj = opNode[0]
                opVal = opNode[1]
                testObj = testNode[0]
                testVal = testNode[1]
                # Need to check both sides
                if self.isObject(ifNode.expr.left, opObj) or self.isObject(ifNode.expr.right, opObj):
                    for id in nodeIds:
                        if hasattr(ifNode.expr.left, id) and self.matchWildcard(opVal, getattr(ifNode.expr.left, id)):
                            op1 = True
                        if hasattr(ifNode.expr.right, id) and self.matchWildcard(opVal, getattr(ifNode.expr.right, id)):
                            op1 = True
                if self.isObject(ifNode.expr.left, testObj) or self.isObject(ifNode.expr.right, testObj):
                    for id in nodeIds:
                        if hasattr(ifNode.expr.left, id) and self.matchWildcard(testVal, getattr(ifNode.expr.left, id)):
                            op2 = True
                        if hasattr(ifNode.expr.right, id) and self.matchWildcard(testVal, getattr(ifNode.expr.right, id)):
                            op2 = True 
                if op1 and op2: 
                    binIfs.append(ifNode)
        return binIfs
    
    def getUserInputs(self, nodes, inputs=None):
        inp = []
        if inputs is None:
            inputs = self.inputs
        for t in inputs:
            obj = t[0]
            if obj == 'func':
                inp.extend(self.getFunctions(nodes, t[1], t[2]))
                # Static too
                inp.extend(self.getFunctions(nodes, t[1], t[2], False))
            else:
                # Either looking for superglobal or plain old var
                if self.isObject(obj, Variable):
                    tl = self.getAllNodes(Variable, nodes)
                    for nd in tl:
                        if self.matchWildcard(t[1], getattr(nd, 'name')):
                            inp.append(nd)
                elif self.isObject(obj, ObjectProperty):
                    op = self.getAllNodes(ObjectProperty, nodes)
                    for nd in op:
                        if self.matchWildcard(t[2], getattr(nd, 'name')):
                            if hasattr(nd.node, 'name') and self.matchWildcard(t[1], getattr(nd.node, 'name')):
                                inp.append(nd)
                elif self.isObject(obj, ArrayOffset) and ':' in t[1]:
                    split = t[1].split(':')
                    nodeType = split[0]
                    nodeName = split[1]
                    if nodeType in self.validVarTypes and nodeType in globals().keys():
                        klass = globals()[nodeType]
                        ao = self.getAllNodes(ArrayOffset, nodes)
                        for nd in ao:
                            if self.isObject(nd.node, klass) and hasattr(nd.node, 'name') and self.matchWildcard(nodeName, getattr(nd.node, 'name')) and self.matchWildcard(t[2], getattr(ao, 'expr')):
                                inp.append(nd)
        return inp

    def getVariableInputs(self, nodes, inputs=None):
        inp = []
        if inputs is None:
            inputs = self.validVarTypes
        for type in inputs:
            inputNodes = self.getAllNodes(type, nodes)
            for inputNode in inputNodes:
                if inputNode not in inp:
                    inp.append(inputNode)
        return inp
    
    def hasUserInput(self, nodes):
        return len(self.getUserInputs(nodes, self.inputs)) > 0
    
    def isObject(self, node, obj):
        if isinstance(node, obj) or node is obj:
            return True
        else:
            return False

    def getAllNodes(self, obj, node, count=0):
        n = []
        # Keep track of recursion count to avoid the limit
        count += 1
        # Return if the limit is about to be hit
        if count >= (sys.getrecursionlimit() - 50):
            return n
        recAttr = False
        if self.isObject(node, obj):
            n.append(node)
            recAttr = True
        else:
            if self.isObject(node, list):
                for nd in node:
                    n.extend(self.getAllNodes(obj, nd, count))
            else:
                recAttr = True
        if recAttr:
            # Deal with Funcception (functions within functions)
            for a in self.validObj[1]:
                if hasattr(node, a):
                    attr = getattr(node, a)
                    if type(attr) in self.validObj[0] or self.isObject(attr, list):
                        n.extend(self.getAllNodes(obj, attr, count))
        return n

    def containsType(self, nodes, types):
        if not self.isObject(types, list):
            types = [types]
        for nodeType in types:
            if self.isObject(nodes, nodeType):
                # Top level node is what were looking for
                return True
            if len(self.getAllNodes(nodeType, nodes)) > 0:
                # Found nested node
                return True
        return False

    def check_danger(self, path, cls, fnodes, local=None):
        for fnc in self.dangerFuncs:
            # Eval is its own node type
            if fnc == 'eval':
                if len(self.getAllNodes(Eval, fnodes)) > 0:
                    self.addResult((fnc, cls, path))
            else:
                if self.hasFunction(fnodes, fnc):
                    self.addResult((fnc.replace('*', ''), cls, path))
                
    def check_includes(self, path, cls, fnodes, local=None):
        varTypes = [Variable, MethodCall, StaticMethodCall, FunctionCall, ObjectProperty]
        incs = self.getAllNodes(Include, fnodes)
        reqs = self.getAllNodes(Require, fnodes)
        for inc in incs:
            if self.containsType(inc.expr, varTypes):
                # Dynamic 
                if inc.once:
                    fname = 'include_once'
                else:
                    fname = 'include'
                self.addResult((fname, cls, path))
        for req in reqs:
            if self.containsType(req.expr, varTypes):
                # Dynamic 
                if req.once:
                    fname = 'require_once'
                else:
                    fname = 'require'
                self.addResult((fname, cls, path))
