from phply.phpast import *
from project import StaticaProject

import re

class Concrete5Project(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['tests', 'build', 'vendor', 'trash', 'cache', 'tmp'])
        self.inputs.extend([('func', 'post', '$this'), ('func', 'get', '$this'), ('func', 'request', '$this'), ('func', 'get', '$session'), ('func', 'post', '*request'), ('func', 'get', '*request'), ('func', 'request', '*request')])
        self.fMatch = ['controller', 'controllers', 'controller.php', 'concrete', 'application', 'tools', 'views', 'jobs', 'single_pages', 'packages', 'themes']
        self.cMatch = ['controller', 'controllers', 'controller.php']
        self.checkConf.extend([
            {'access': {
                'filter': ['web'],
                'check_parent': False,
                'context': 'outer',
                'name_pattern': None,
                'modifiers': None
            }},
            {'csrf': {
                'filter': self.cMatch,
                'check_parent': True,
                'context': 'inner',
                'name_pattern': '^[a-zA-Z0-9]+[a-zA-Z0-9_]*$',
                'modifiers': None
            }},
            {'xss': {
                'filter': self.cMatch + ['concrete'],
                'check_parent': False,
                'context': 'inner',
                'name_pattern': None,
                'modifiers': None
            }},
            {'sqli': {
                'filter': self.cMatch + ['concrete'],
                'check_parent': False,
                'context': 'inner',
                'name_pattern': None,
                'modifiers': None
            }},
        ])
                
    def check_access(self, path, cls, fnodes):
        fn = self.getFunctions(fnodes, 'defined')
        foundC5Exec = []
        for f in fn:
            if hasattr(f, 'params') and f.name == 'defined':                 
                for p in self.getFuncParams(f):
                    if p.node == 'C5_EXECUTE':
                        foundC5Exec.append(True)
        if True not in foundC5Exec:
            self.addResult(('n/a', cls, path))
                
    def check_csrf(self, path, cls, fnodes):
        foundCSRFChk1 = False
        foundCSRFChk2 = False
        foundCSRFChk3 = False
        foundCSRFChk4 = False
        foundCSRFChk5 = False
        inp = self.getUserInputs(fnodes, self.inputs)
        if len(inp) == 0 and len(self.getFuncParams(fnodes)) == 0:
            return
        if self.hasFunction(fnodes, 'validate', '*token') or self.hasFunction(fnodes, '_csrfProtection') or self.hasFunction(fnodes, 'validatePageRequest'):
            foundCSRFChk1 = True
        if self.getFunction(fnodes, 'helper', 'validation/token', 'Loader', False) is not None or self.hasFunction(fnodes, 'addRequiredToken'):
            foundCSRFChk2 = True
        if self.getFunction(fnodes, 'make', 'helper/validation/token', 'Core', False) is not None or self.getFunction(fnodes, 'make', 'helper/validation/token') is not None:
            foundCSRFChk3 = True
        if self.hasFunction(fnodes, 'validateKey') or self.hasFunction(fnodes, 'validate', '$validation_token') or self.hasFunction(fnodes, 'validateAction', '$this'):
            foundCSRFChk4 = True
        if self.getFunction(fnodes, 'make', 'token', 'Core', False) is not None or self.getFunction(fnodes, 'make', 'token') is not None:
            foundCSRFChk5 = True
        if (foundCSRFChk1 is False and foundCSRFChk2 is False and foundCSRFChk3 is False and foundCSRFChk4 is False and foundCSRFChk5 is False):
            self.addResult((fnodes.name, cls, path))

    def check_xss(self, path, cls, fnodes):
        # Only use globals for input
        ns = self.getUserInputs(fnodes, [(Variable, '$_REQUEST', None), (Variable, '$_GET', None), (Variable, '$_POST', None)])
        for n in ns:
            # Check for global
            if self.isObject(n, Variable):
                self.addResult((fnodes.name, cls, path))
        
    def check_sqli(self, path, cls, fnodes):
        san = []
        ex = []
        inp = self.getUserInputs(fnodes, self.inputs)
        if len(inp) == 0 and len(self.getFuncParams(fnodes)) == 0:
            return
        san.extend(self.getFunctions(fnodes, 'sanitize*'))
        # Look for old ADODb funcs
        ex.extend(self.getFunctions(fnodes, 'Execute'))
        ex.extend(self.getFunctions(fnodes, 'Prepare'))
        ex.extend(self.getFunctions(fnodes, 'Get*'))
        ex.extend(self.getFunctions(fnodes, 'Fetch*'))
        # New doctrine way too
        ex.extend(self.getFunctions(fnodes, 'query'))
        ex.extend(self.getFunctions(fnodes, 'fetch*'))
        ex.extend(self.getFunctions(fnodes, 'delete'))
        ex.extend(self.getFunctions(fnodes, 'insert'))
        ex.extend(self.getFunctions(fnodes, 'update'))
        if len(ex) > 0 and len(san) < 1:
            self.addResult((fnodes.name, cls, path))

