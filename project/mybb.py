from phply.phpast import *
from project import StaticaProject

class MyBBProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['cache', 'images', 'install'])
        self.inputs.extend([(ObjectProperty, '*mybb', 'input'), (ObjectProperty, '*mybb', 'cookies'), ('func', 'get_input', '*mybb')])
        self.fMatch = ['.php']
        self.cMatch = ['class_']
        self.valFuncs.extend([('*db', 'escape*', True), (None, 'clean_*', True)])
        self.checkConf.extend([
            {'csrf': {'filter': self.fMatch, 'check_parent': 0}},
            {'xss': {'filter': self.fMatch, 'check_parent': 0}},
            {'sqli': {
                'filter': self.fMatch, 
                'check_parent': False,
                'context': 'outer',
                'name_pattern': None,
                'modifiers': None
            }},
        ])
        self.ifBlocks = ['node', 'elseifs', 'else_']
        self.castFilters = ['INPUT_INT', 'INPUT_FLOAT', 'INPUT_BOOL']
        
    def getCastFilters(self, nodes):
        filters = []
        consts = self.getAllNodes(StaticProperty, nodes)
        for const in consts:
            if const.node == 'MyBB' and const.name in self.castFilters:
                filters.append(const)
        return filters
        
    def check_xss(self, path, cls, fnodes, local=None):
        ifs = self.getBinaryIfs(fnodes, '==', (ArrayOffset, 'action'), (str, '*'))
        for ifNode in ifs:
            for ifBlock in self.ifBlocks:
                nodes = getattr(ifNode, ifBlock)
                san = self.getCastFilters(nodes)
                inp = self.getUserInputs(nodes, self.inputs)
                out = self.getFunctions(nodes, '*output*')
                out.extend(self.getFunctions(nodes, '*generate*'))
                out.extend(self.getFunctions(nodes, '*construct*'))
                if len(inp) > 0 and len(out) > 0 and len(san) < inp:
                    if hasattr(fnodes, 'name'):
                        name = fnodes.name
                    else:
                        if self.isObject(ifNode.expr.right, str):
                            name = 'action = "%s"' % (ifNode.expr.right)
                        else:
                            name = 'n/a'
                    self.addResult((name, cls, path))
            
    def check_sqli(self, path, cls, fnodes, local=None):
        quoteFuncs = []
        dbFuncs = []
        quoteFuncs.extend(self.getFunctions(fnodes, 'escape*', '*db'))
        dbFuncs.extend(self.getFunctions(fnodes, '*query', '*db'))
        dbFuncs.extend(self.getFunctions(fnodes, 'fetch*', '*db'))
        dbFuncs.extend(self.getFunctions(fnodes, 'simple_select', '*db'))
        for dbFunc in dbFuncs:
            bOp = self.getAllNodes(BinaryOp, dbFunc)
            # Only concat ops
            bOpConcat = [op for op in bOp if op.op == '.']
            if len(bOpConcat) > 0:
                hasQuote = False
                for quoteFunc in quoteFuncs:
                    if self.inScopeOf(quoteFunc, dbFunc):
                        hasQuote = True
                        break
                # Also check cast filters
                if hasQuote is False:
                    for castFilter in self.getCastFilters(fnodes):
                        if self.inScopeOf(castFilter, dbFunc):
                            hasQuote = True
                            break
                if hasQuote is False:
                    vars = self.getVariableInputs(bOp)
                    for var in vars:
                        varState = self.getVarState(var, statenodes=vars)
                        if varState is not None:
                            if varState.wasCast() is False and varState.wasValidated() is False:
                                if hasattr(fnodes, 'name'):
                                    name = '%s(%s)' % (fnodes.name, varState.key.replace('\'', '"'))
                                else:
                                    name = varState.key.replace('\'', '"')
                                self.addResult((name, cls, path))
            
    def check_csrf(self, path, cls, fnodes, local=None):
        ifs = self.getBinaryIfs(fnodes, '==', (ArrayOffset, 'action'), (str, '*'))
        for ifNode in ifs:
            for ifBlock in self.ifBlocks:
                nodes = getattr(ifNode, ifBlock)
                san = self.getFunctions(nodes, 'verify_post_check')
                inp = self.getUserInputs(nodes, self.inputs)
                if len(inp) > 0 and len(san) < inp:
                    if hasattr(fnodes, 'name'):
                        name = fnodes.name
                    else:
                        if self.isObject(ifNode.expr.right, str):
                            name = 'action = "%s"' % (ifNode.expr.right)
                        else:
                            name = 'n/a'
                    self.addResult((name, cls, path))  
