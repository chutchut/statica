from phply.phpast import *
from project import StaticaProject


class JoomlaProject(StaticaProject):
    def __init__(self, name, version, check, conf, inst, db, obj):
        StaticaProject.__init__(self, name, version, check, conf, inst, db, obj)
        self.excludedPaths.extend(['tests', 'build', 'vendor'])
        self.inputs.extend([('func', 'get', '*input'), ('func', 'get', '*get'), ('func', 'get', '*post'), ('func', 'getState', None), ('func', 'getUserState', None), ('func', '*FromRequest', None)])
        self.fMatch = ['Controller', 'controller', 'controllers', 'controller.php', 'components', 'models', 'helpers', 'views', 'layouts', 'libraries', 'modules', 'plugins', 'templates']
        self.cMatch = ['Controller', 'controller', 'controllers', 'controller.php']
        self.valFuncs.extend([('*db*', 'quote*', True), ('*db*', 'q', True), ('*db*', 'escape', True), ('*query*', 'cast*', True), ('ArrayHelper', 'toInteger', False)])
        self.checkConf.extend([
            {'access': {
                'filter': ['administrator/components'],
                'check_parent': False,
                'context': 'outer.nonfunc',
                'name_pattern': None,
                'modifiers': None
            }},
            {'csrf': {
                'filter': self.cMatch,
                'check_parent': True,
                'context': 'inner',
                'name_pattern': '^[a-zA-Z0-9]+[a-zA-Z0-9_]*$',
                'modifiers': ['public']
            }},
            {'xss': {
                'filter': self.cMatch + ['components'],
                'check_parent': False,
                'context': 'inner',
                'name_pattern': None,
                'modifiers': None
            }},
            {'sqli': {
                'filter': ['joomla/components'],
                'check_parent': False,
                'context': 'inner',
                'name_pattern': None,
                'modifiers': None
            }},
        ])
        self.classAlias = {
            'JControllerLegacy': 'BaseController',
            'JControllerForm': 'FormController',
            'JControllerAdmin': 'AdminController',
            'JObject': 'CMSObject',
            'JFormField': 'FormField',
            'JPlugin': 'CMSPlugin',
            'JViewLegacy': 'HtmlView',
            'JComponentRouterView': 'RouterView',
            'JModelList': 'ListModel',
            'JComponentRouterBase': 'RouterBase',
            'JFormRule': 'FormRule',
            'JModelAdmin': 'AdminModel',
            'JModelForm': 'FormModel',
            'JTable': 'Table',
            'JHelperContent': 'ContentHelper',
            'JModelLegacy': 'BaseDatabaseModel'
        }
        
    def check_access(self, path, cls, fnodes, local=None):
        fn = self.getFunctions(fnodes, 'defined')
        foundJExec = []
        foundPlatform = []
        foundBase = [] 
        for f in fn:
            if hasattr(f, 'params') and f.name == 'defined':                 
                for p in self.getFuncParams(f):
                    if p.node == '_JEXEC':
                        foundJExec.append(True)
                    if p.node == 'JPATH_PLATFORM':
                        foundPlatform.append(True)
                    if p.node == 'JPATH_BASE':
                        foundBase.append(True)
        if True not in foundJExec and True not in foundPlatform and True not in foundBase:
            self.addResult(('n/a', cls, path))
                
    def check_csrf(self, path, cls, fnodes, local=None):
        foundCSRFChk = False
        if self.hasFunction(fnodes, 'checkToken', '*JSession', False) or self.hasFunction(fnodes, 'checkToken') or self.hasFunction(fnodes, '_csrfProtection'):
            foundCSRFChk = True
        if foundCSRFChk is False:
            self.addResult((fnodes.name, cls, path))
    
    def check_xss(self, path, cls, fnodes, local=None):
        ns = self.getUserInputs(fnodes, self.inputs)
        report = False
        for n in ns:
            if hasattr(n, 'params'):
                # Lower case then compare
                for p in n.params:
                    if str(p.node).lower() == 'raw':
                        report = True
            # Check for global
            if self.isObject(n, Variable):
                report = True
        if report:
            self.addResult((fnodes.name, cls, path))
        
    def check_sqli(self, path, cls, fnodes, local=None):
        dbFuncs = []
        dbFuncs.extend(self.getFunctions(fnodes, 'select'))
        dbFuncs.extend(self.getFunctions(fnodes, 'update'))
        dbFuncs.extend(self.getFunctions(fnodes, 'delete'))
        dbFuncs.extend(self.getFunctions(fnodes, 'from'))
        dbFuncs.extend(self.getFunctions(fnodes, 'where'))
        dbFuncs.extend(self.getFunctions(fnodes, 'order'))
        dbFuncs.extend(self.getFunctions(fnodes, 'union'))
        dbFuncs.extend(self.getFunctions(fnodes, '*Where'))
        for dbFunc in dbFuncs:
            bOp = self.getAllNodes(BinaryOp, dbFunc)
            # Only concat ops
            bOpConcat = [op for op in bOp if op.op == '.']
            if len(bOpConcat) > 0:
                vars = self.getVariableInputs(bOpConcat)
                for var in vars:
                    varState = self.getVarState(var)
                    if varState is not None:
                        if varState.wasCast() is False and varState.wasValidated() is False:
                            self.addResult(('%s(%s)' % (fnodes.name, varState.key.replace('\'', '"')), cls, path))

