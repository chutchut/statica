#!/usr/bin/python

import sys, os, traceback, re, time, hashlib, base64, imp, pickle
from blessings import Terminal
import ConfigParser
import sqlite3
import threading
import argparse

from subprocess import Popen, PIPE

import phply.phpast
from phply.phpparse import make_parser
import inspect

from project import *

MAIN_CONF = 'conf/statica.conf'

class Statica():
    def __init__(self, repProj, conf=None, mode='s', report=False, startAt=None, reload=False, onlyCheck=None, showErrors=False, useCheckThreads=False, numCheckThreads=10, noInit=False):
        self.__confObj = StaticaConfig(conf)
        self.__conf = self.__confObj.load()
        self.__db = None
        self.__handledObjects = []
        self.__phplyParser = None
        self.alwaysReport = report
        self.reportProj = repProj
        self.startAt = startAt
        self.reload = reload
        self.onlyCheck = onlyCheck
        self.showErrors = showErrors
        self.proj = None
        self.loop = 0
        self.mode = mode
        self.dispatcher = StaticaDispatch(None)
        self.numCheckThreads = numCheckThreads
        self.useCheckThreads = useCheckThreads
        self.workers = []
        self.classCache = {}
        if noInit is False:
            self.init()
        
    def init(self):
        # Check root path
        if os.path.exists(self.getGlobalConf('git_root_path')) is False:
            print('Root path (%s) does not exist, creating now..' % (self.getGlobalConf('git_root_path')))
            os.mkdir(self.getGlobalConf('git_root_path'))
        # Check db
        self.__db = StaticaDb(self.getGlobalConf('db_path'))
        if os.path.exists(self.getGlobalConf('db_path')) is False:
            print('SQlite db (%s) does not exist, creating now..' % (self.getGlobalConf('db_path')))
            self.__db.init()
        self.__db.connect()
        # Check for the parsetab file and phply module folder perms
        phplyDir = os.path.dirname(phply.phpast.__file__)
        if os.path.exists(phplyDir + '/parsetab.py') is False:
            if os.access(phplyDir, os.W_OK) is False:
                print('No write access to module path (%s), please set permissions so that the path is writable or run the script as root to create the parsetab cache' % (phplyDir))
                sys.exit(3)
            else:
                print('Creating parsetab cache in module path: %s' % (phplyDir))
        # Create a parser
        self.__phplyParser = make_parser()
        # Load handled object attributes for phply
        self.__handledObjects = self.getObjectsToParse()
        # Load projects (get from git if nec)
        self.proj = self.loadProjects() 
        # Override project checks in config with cli param if defined
        if self.onlyCheck is not None:
            for p in self.proj:
                p['check'] = self.onlyCheck
        # Initialise the db with the project data if it doesnt exist
        self.initDb(self.proj)
        self.dispatcher.dispatch('init_done', self.proj)
        
    def initDb(self, projData):
        print('Initialising db..')
        for p in projData:
            # Projects
            if self.__db.getProject(p['name']) is None:
                self.__db.query('''INSERT INTO project (name) VALUES ('%s')''' % (p['name']))
            # Versions
            if not p['standalone']:
                if p['check_freq'] == 'tag':
                    for t in p['tag']:
                        if self.__db.getVersion(t) is None:
                            self.__db.query('''INSERT INTO version (name, type) VALUES ('%s', 'tag')''' % (t))
                if p['check_freq'] == 'commit':
                    for c in p['commit']:
                        if self.__db.getVersion(c) is None:
                            self.__db.query('''INSERT INTO version (name, type, branch) VALUES ('%s', 'commit', '%s')''' % (c, p['commit_branch']))
            else:
                sa = 'STANDALONE'
                if self.__db.getVersion(sa) is None:
                    self.__db.query('''INSERT INTO version (name, type) VALUES ('%s', 'standalone')''' % (sa))
            # Check types
            typeStr = p['check']
            types = typeStr.split(',')
            for t in types:
                if self.__db.getType(t) is None:
                    self.__db.query('''INSERT INTO type (name) VALUES ('%s')''' % (t))
        
    def getObjectsToParse(self):
        obj = []
        attr = []
        md = inspect.getmembers(phply.phpast)
        print('Getting attributes to parse..')
        for m in md:
            if inspect.isclass(m[1]):
                #print('Reading attributes for: %s' % (m[0]))
                obj.append(m[1])
                for f in m[1].fields:
                    if f not in attr:
                        attr.append(f)
        return (obj, attr)
    
    def getHandledObjects(self):
        return self.__handledObjects

    def getParser(self):
        return self.__phplyParser
        
    def loadProjects(self):
        proj = []
        if self.reportProj is None:
            for p in self.getProjectConf():
                proj.append(self.loadProject(p, self.__db))
        else:
            for p in self.getProjectConf():
                if p['name'].lower() == self.reportProj.lower():
                    proj.append(self.loadProject(p, self.__db))
        return proj
    
    def loadProject(self, proj, db):
        # Check for standalone project (no git req)
        if not proj['standalone']:
            sg = StaticaGit(self.getGlobalConf('git_root_path'), proj)
            if os.path.exists('%s/%s' % (self.getGlobalConf('git_root_path'), proj['name'].lower())) is False:
                print('Creating git repo for project: %s' % (proj['name']))
                sg.clone()
        else:
            if os.path.exists(proj['clone_url']) is False:
                print('Standalone project path does not exist: %s' % (proj['clone_url']))
                sys.exit(2)
            self.mode = 's'
            print('Standalone project, source is located at: %s (no git required)' % (proj['clone_url']))
        # If reload is set, remove project data first
        if self.reload:
            checks = None
            try:
                # Prompt fist to avoid accidents..
                if self.onlyCheck is not None:
                    checks = self.onlyCheck.split(',')
                    msg = '[Enter] to confirm project reload or Ctrl+C to exit.. (reloading %s data)' % (checks)
                else:
                    msg = '[Enter] to confirm project reload or Ctrl+C to exit..'
                raw_input(msg)
                if self.reloadProject(proj['name'], checks) is not True:
                    raw_input('Problem reloading data, are you sure you want to continue..?')
            except KeyboardInterrupt:
                sys.exit(4)
        if not proj['standalone']:
            verFilter = self.getVersionFilter()
            exact = False
            filter = verFilter[0]
            rollbackNum = verFilter[1]
            if filter is not None and rollbackNum is not None:
                exact = True
                rollbackNum = None
            if proj['check_freq'] == 'tag':
                gitTags = sg.getTags(filter, rollbackNum)
                # Get db tags
                dbTags = db.getTags(proj['name'])
                proj['git'] = sg
                proj['tag'] = list(set(gitTags) - set(dbTags))
                proj['tag'].sort()
            if proj['check_freq'] == 'commit':
                gitComs = sg.getCommits(filter, rollbackNum)
                # Get db commits
                dbComs = db.getCommits(proj['name'])
                proj['git'] = sg
                proj['commit'] = list(set(gitComs) - set(dbComs))
                proj['commit'].sort()
            if exact:
                # Exact version
                if filter in proj[proj['check_freq']]:
                    proj[proj['check_freq']] = [filter]
                else:
                    proj[proj['check_freq']] = []
        return proj
    
    def reloadProject(self, name, checks=None):
        # Get proj id
        pRec = self.__db.getProject(name)
        if pRec is None:
            return True
        if checks is not None:
            # Get type ids for the checks
            tIds = []
            for c in checks:
                tRec = self.__db.getType(c)
                if tRec is not None:
                    tIds.append(tRec[0])
        pID = pRec[0]
        if checks is None:
            print('Reloading project data for %s..' % (name))
            q = '''DELETE FROM check_metadata WHERE proj_id = %d''' % (pID)
            self.__db.query(q)
            q = '''DELETE FROM parse_metadata WHERE proj_id = %d''' % (pID)
            self.__db.query(q)
            q = '''DELETE FROM issue WHERE proj_id = %d''' % (pID)
            self.__db.query(q)
            q = '''DELETE FROM project_to_version WHERE proj_id = %d''' % (pID)
            self.__db.query(q)
            q = '''DELETE FROM file_hashmap WHERE project = '%s\'''' % (name)
            self.__db.query(q)
            return True
        else:
            if len(tIds) > 0:
                print('Reloading project data for %s (%s)..' % (name, checks))
                for tId in tIds:
                    q = '''DELETE FROM check_metadata WHERE proj_id = %d AND type_id = %d''' % (pID, tId)
                    self.__db.query(q)
                    q = '''DELETE FROM issue WHERE proj_id = %d AND type_id = %d''' % (pID, tId)
                    self.__db.query(q)
                q = '''DELETE FROM project_to_version WHERE proj_id = %d''' % (pID)
                self.__db.query(q)
                q = '''DELETE FROM file_hashmap WHERE project = '%s\'''' % (name)
                self.__db.query(q)
                return True
            else:
                print('Couldnt find any valid check types')
                return False
        
    def __loadProjectModule(self, proj):
        projClassPath = 'project/%s.py' % (proj)
        if not os.path.exists(projClassPath):
            print('Project class file missing, expected path: %s' % (projClassPath))
            sys.exit(5)
        return imp.load_source('project.%s' % (proj), projClassPath)
    
    def getObject(self, p, ver, db):
        poName = '%sProject' % (p['name'])
        pMod = self.__loadProjectModule(p['name'].lower())
        class_ = getattr(pMod, poName)
        po = class_(p['name'], ver, p['check'], p, self, db, self.__handledObjects)
        return po

    def getProjectCheckObject(self, project_name, version):
        pConf = self.getProjectConf(project_name)
        if pConf is not None and version in pConf[pConf['check_freq']]:
            return self.getObject(pConf, version, self.getDb())
        else:
            return None
    
    def getVersionFilter(self):
        filter = None
        rollbackNum = None
        if self.startAt is not None:
            rollbackNumMatch = re.search('^-(\d+)$', self.startAt)
            if rollbackNumMatch is not None:
                rollbackNum = rollbackNumMatch.group(1)
            else:
                exactMatch = re.search('^<([\w\.\-_]+)>$', self.startAt)
                if exactMatch is not None:
                    filter = exactMatch.group(1)
                    rollbackNum = -1
                else:
                    filter = self.startAt
        return (filter, rollbackNum)
    
    def run(self):
        self.dispatcher.dispatch('start', None)
        if len(self.proj) < 1:
            print('No projects found!')
            self.dispatcher.dispatch('finish', None)
            return
        for p in self.proj:
            self.dispatcher.dispatch('proj_start', p)
            try:
                if not p['standalone']:
                    if p['sync_freq'].strip() != '':
                        int = self.getInterval(p['sync_freq'])
                    else:
                        int = self.getInterval(self.getGlobalConf('sync_freq'))
                if self.mode == 'd':
                    sw = StaticaWorker(int, self.projLoop, self.mode, p)
                    self.workers.append(sw)
                    sw.start()
                if self.mode == 's':
                    self.projLoop(p, worker=None)
            except Exception as e:
                print('Exception in run(): %s' % (e))
                traceback.print_exc()
            self.dispatcher.dispatch('proj_finish', p)
        self.dispatcher.dispatch('finish', None)
                
    def isRunning(self):
        running = False
        for w in self.workers:
            if w.running is True:
                running = True
        return running
                
    def stop(self):
        print('Stopping worker threads..')
        for w in self.workers:
            w.stop()
                
    def report(self, proj, db):
        t = Terminal()
        # Get versions (tags/commits) that were checked on this project
        vers = self.__db.getProjectVersions(proj['name'], proj['check_freq'])
        # Report on enabled checks
        checks = self.getReportChecks(proj['name'])
        iss = self.getAllIssues(proj['name'], vers)
        fix = []
        for projVer in vers:
            print('Report for %s version %s' % (proj['name'], projVer))
            print('--------------------------------')
            for k in checks:
                print('')
                print(k.upper())
                print('-' * len(k))
                for r in iss[k][projVer][0]:
                    print(t.cyan('[%s] Function: %s :: Class: %s :: File: %s' % (k.upper(), r[0], r[1], r[2])))
                for r in iss[k][projVer][1]:
                    if r not in fix:
                        print(t.green('[%s - Fix] Function: %s :: Class: %s :: File: %s' % (k.upper(), r[0], r[1], r[2])))
                        fix.append(r)
            self.getStats(proj['name'], projVer)
        if self.mode == 's':
            # Get remaining issues
            print('Remaining issues for %s' % (proj['name']))
            print('--------------------------------')
            rIss = self.getRemainingIssues(proj['name'], vers)
            for k in checks:
                print('')
                print(k.upper())
                print('-' * len(k))
                for r in rIss[k]:
                    print(t.cyan('[%s] Version: %s :: Function: %s :: Class: %s :: File: %s' % (k.upper(), r[3], r[0], r[1], r[2])))
            print('')
            
    def getProjectData(self, proj, type='tag', version=None):
        res = {}
        iData = self.__db.getIssueData(proj, type, version)
        checks = self.getReportChecks(proj)
        for c in checks:
            res[c] = []
            for i in iData:
                if i[10] == c:
                    res[c].append(i)
        return res
    
    def getSortedIssues(self, iss):
        res = {}
        none, confirm, false, investigate = {}, {}, {}, {}
        for k in iss.keys():
            res[k] = {}
            res[k]['none'] = []
            res[k]['confirm'] = []
            res[k]['false'] = []
            res[k]['investigate'] = []
            for i in iss[k]:
                r = self.__db.getIssue(i[0], i[1], i[2], k, True, i[3])    
                if len(r) < 1:
                    res[k]['none'].append(i)
                else:
                    for ai in r:
                        if ai[8] == None:
                            res[k]['none'].append((ai[3], ai[4], ai[5], ai[7]))
                        if int(ai[8]) == 0:
                            res[k]['none'].append((ai[3], ai[4], ai[5], ai[7]))
                        if int(ai[8]) == 1:
                            res[k]['confirm'].append((ai[3], ai[4], ai[5], ai[7]))
                        if int(ai[8]) == 2:
                            res[k]['false'].append((ai[3], ai[4], ai[5], ai[7])) 
                        if int(ai[8]) == 3:
                            res[k]['investigate'].append((ai[3], ai[4], ai[5], ai[7])) 
        return res
            
    def getRemainingIssues(self, proj, fromVersions=[]):
        # Get all issues
        aIss = self.getAllIssues(proj, fromVersions)
        checks = self.getReportChecks(proj)
        rem = {}
        for c in checks:
            found = []
            cur, fix, remlst = [], [], []
            for ver in aIss[c].keys():
                curIss = aIss[c][ver][0]
                oldFix = aIss[c][ver][1]
                for r in curIss:
                    cur.append(','.join(r))
                for r in oldFix:
                    fix.append(','.join(r))
            remIss = list(set(cur) - set(fix))
            for r in remIss:
                iVer = None
                rSplit = r.split(',')
                if ','.join([rSplit[0], rSplit[1], rSplit[2]]) in found:
                    continue
                for ver in aIss[c].keys():
                    for i in aIss[c][ver][0]:
                        if hash(','.join(i)) == hash(r):
                            iVer = ver
                            break
                remlst.append((rSplit[0], rSplit[1], rSplit[2], iVer))
                found.append(','.join([rSplit[0], rSplit[1], rSplit[2]]))
            # Sort by path
            remlst.sort(key=lambda x: x[2])
            # Sort by version
            remlst.sort(key=lambda x: x[-1])
            rem[c] = remlst
        return rem
            
    def getAllIssues(self, proj, fromVersions=[]):
        aIss = {}
        if len(fromVersions) == 0:
            conf = self.getProjectConf(proj)
            if not conf['standalone']:
                fromVersions = self.__db.getProjectVersions(proj, conf['check_freq'])
            else:
                fromVersions = ['STANDALONE']
        for ver in fromVersions:
            vData = self.getIssues(proj, ver)
            for k in vData.keys():
                if k not in aIss.keys():
                    aIss[k] = {}
                if ver not in aIss[k].keys():
                    aIss[k][ver] = ([], [])
                aIss[k][ver][0].extend(vData[k][0])
                aIss[k][ver][1].extend(vData[k][1])
        return aIss
        
    def getIssues(self, proj, version):
        iss = {}
        index = self.__getVersionIndex(proj, version)
        checks = self.getReportChecks(proj)
        curData = self.getCurrentIssueData(proj, version)
        conf = self.getProjectConf(proj)
        
        for c in checks:
            if c not in iss.keys():
                iss[c] = {}
            if index == 0:
                # First version, get all results
                iss[c] = (curData[c], [])
            else:
                # Diff between prev version(s)
                new = self.__db.diffIssues(proj, version, c, 'new', conf['check_freq']) 
                fix = self.__db.diffIssues(proj, version, c, 'fix', conf['check_freq']) 
                iss[c] = (new, fix)
        return iss
    
    def getCurrentIssueData(self, proj, version):
        # Get current data
        conf = self.getProjectConf(proj)
        if not conf['standalone']:
            cData = self.getProjectData(proj, conf['check_freq'], version)
        else:
            cData = self.getProjectData(proj, 'standalone', version)
        iss = {}
        for k in cData.keys():
            iss[k] = []
            for i in cData[k]:
                iss[k].append((i[4], i[5], i[6]))
        return iss
    
    def getPreviousIssueData(self, proj, version):
        # Get previous data
        index = self.__getVersionIndex(proj, version)
        checks = self.getReportChecks(proj)
        pData = self.getIssueData(proj, checks, version)
        iss = {}
        for k in pData.keys():
            iss[k] = []
            for i in pData[k]:
                iss[k].append((i[4], i[5], i[6]))
        return iss
    
    def getIssueData(self, proj, checks, excludeVer=None):
        data = {}
        conf = self.getProjectConf(proj)
        versions = self.__db.getProjectVersions(proj, conf['check_freq'])
        for c in checks:
            data[c] = []
        for ver in versions:
            if excludeVer is not None and excludeVer == ver:
                continue
            iData = self.getProjectData(proj, conf['check_freq'], ver)
            for c in checks:
                for i in iData[c]:
                    if i not in data[c]:
                        data[c].append(i)
        return data
    
    def __getVersionIndex(self, proj, version):
        index = 0
        conf = self.getProjectConf(proj)
        versions = self.__db.getProjectVersions(proj, conf['check_freq'])
        for i in range(0, len(versions)):
            if versions[i] == version:
                index = i
                break
        return index
    
    def getIssueHash(self, version, type, func, cls, path):  
        m = hashlib.md5()
        m.update('%s:%s:%s:%s:%s' % (version, type, func, cls, path))
        return m.hexdigest()
    
    def getReportVersions(self, projData, type):
        vers = []
        for c in projData.keys():
            for i in projData[c]:
                if i[9] == type and i[8] not in vers:
                    vers.append(i[8])
        return vers
                    
    def getReportChecks(self, proj):
        pConf = self.getProjectConf(proj)
        return pConf['check'].split(',')
    
    def getErrors(self, proj, version):
        pData = self.__db.getParseMetadata(proj, version)
        return pickle.loads(base64.b64decode(pData[8]))
    
    def getStats(self, proj, version):
        print('')
        cData = self.__db.getCheckMetadata(proj, version)
        pData = self.__db.getParseMetadata(proj, version)
        duration = float(pData[3])
        total = float(pData[4])
        parsed = float(pData[5])
        skipped = float(pData[6])
        errors = self.getErrors(proj, version)
        if parsed > 0 and total > 0:
            parsePct = (parsed / total) * 100
        else:
            parsePct = 0.0
        eTotal = 0.0
        print('Stats: (Version %s)' % (version))
        print('-----')
        print('Parse Duration: %.2fs' % (duration))
        for c in cData:
            eTotal += float(c[4])
            print('Check [%s] duration: %.2fs' % (c[8], float(c[4])))
        durTotal = (eTotal + float(duration))
        elapsedMin = (durTotal / 60)
        print('Total Duration: %.2fs (%.2fm)' % (durTotal, elapsedMin))
        print('Total lines: %d' % (total))
        print('Skipped: %d' % (skipped))
        print('Parsed: %d (%.2f%%)' % (parsed, parsePct))
        if self.showErrors:
            print('')
            print('Errors')
            print('------')
            for e in errors:
                print(e)
        print('')
        
    def projLoop(self, proj, worker=None):
        db = StaticaDb(self.getGlobalConf('db_path'))
        db.connect()
        if not proj['standalone']:
            key = proj['check_freq']
            if self.mode != 's' and self.loop > 0:
                pr = self.loadProject(proj, db)
                if pr[key] != proj[key] and len(pr[key]) > 0:
                    print('New %s found, reloading objects' % (key))
                    proj = pr
            if len(proj[key]) > 0:
                for e in proj[key]:
                    if worker is None or worker.running:
                        o = self.getObject(proj, e, db)
                        if key == 'tag':
                            proj['git'].checkout(o.version)
                        if key == 'commit':
                            proj['git'].checkout(proj['commit_branch'])
                        o.check()
                self.report(proj, db)
                self.loop += 1
            else:
                print('No new git %ss to check for project: %s' % (key, proj['name']))
                if self.mode == 's' and self.alwaysReport:
                    self.report(proj, db)
        else:
            o = self.getObject(proj, 'STANDALONE', db)
            if len(db.getProjectVersions(proj['name'], 'standalone')) < 1:
                o.check()
                self.report(proj, db)
            if self.mode == 's' and self.alwaysReport:
                self.report(proj, db)
            
    def getGlobalConf(self, name):
        return self.__conf['global'][name]
        
    def getProjectConf(self, proj=None):
        if proj is None:
            # Return all projects
            return self.__conf['projects']
        else:
            pr = None
            for p in self.__conf['projects']:
                if p['name'].lower() == proj.lower():
                    pr = p
            return pr
        
    def getConf(self):
        return self.__conf
    
    def getConfObj(self):
        return self.__confObj
    
    def getDb(self):
        return self.__db
    
    def getInterval(self, val):
        valMatch = re.search('(\d+)(\w)', val)
        if valMatch is not None:
            if valMatch.group(2) == 'm':
                return int(valMatch.group(1)) * 60
            if valMatch.group(2) == 'h':
                return int(valMatch.group(1)) * 60 * 60
        else:
            raise Exception('Invalid time format')
        
    def setDispatcher(self, disp):
        self.dispatcher.setInstance(disp)
        
class StaticaDispatch():
    def __init__(self, inst):
        self.__inst = inst
        
    def setInstance(self, inst):
        self.__inst = inst
        
    def getInstance(self):
        return self.__inst
    
    def dispatch(self, event, data):
        try:
            if self.__inst is not None:
                func = getattr(self.__inst, 'on_%s' % (event))
                func(data)
        except Exception as e:
            print(e)
            pass
    
class StaticaWorker(threading.Thread):
    def __init__(self, interval, func, mode='s', *args, **kwargs):
        threading.Thread.__init__(self)
        self.interval = interval  
        self.func = func
        self.mode = mode
        self.args = args
        self.kwargs = kwargs
        self.running = False
        
    def run(self):
        self.running = True
        while self.running:
            try:
                self.func(worker=self, *self.args, **self.kwargs)
                if self.mode == 's':
                    self.running = False
                else:
                    time.sleep(self.interval)
            except Exception as e:
                print('Exception running worker thread: %s' % (e))
                traceback.print_exc()
                self.running = False
            
    def stop(self):
        self.running = False
    
class StaticaGit():
    def __init__(self, root, proj):
        self.cli = StaticaCli()
        self.projPath = '%s/%s' % (root, proj['name'].lower())
        self.proj = proj
        
    def clone(self):
        p = self.cli.command('git clone %s %s' % (self.proj['clone_url'], self.projPath), True)
        print('Cloning into: %s' % (self.projPath))
        self.cli.printOutput(self.cli.getOutputLines(p))
        
    def getTags(self, tagFilter=None, rollback=None):
        tags = []
        print('Getting tags for repo: %s' % (self.projPath))
        p = self.cli.command('git --git-dir=%s/.git fetch --tags' % (self.projPath), True)
        self.cli.printOutput(self.cli.getOutputLines(p))
        p = self.cli.command('git --git-dir=%s/.git tag' % (self.projPath), True)
        allTags = self.cli.getOutputLines(p)
        p = self.cli.command('git --git-dir=%s/.git describe --tags $(git --git-dir=%s/.git rev-list --tags --max-count=1)' % (self.projPath, self.projPath), True)
        latestTag = self.cli.getOutputLines(p)[0]
        startTagFound = False
        endTagFound = False
        for t in allTags:
            if re.match(self.proj['tag_filter'], t) is not None:
                if tagFilter is None:
                    if t == self.proj['tagstart']:
                        startTagFound = True
                else:
                    if t == tagFilter:
                        startTagFound = True
                if startTagFound and not endTagFound:
                    tags.append(t)
            if t == latestTag:
                endTagFound = True
        if rollback is None:
            return tags
        else:
            return tags[-int(rollback):]
    
    def getCommits(self, comFilter=None, rollback=None):
        commits = []
        print('Getting commits for repo: %s' % (self.projPath))
        p = self.cli.command('git --git-dir=%s/.git fetch' % (self.projPath), True)
        self.cli.printOutput(self.cli.getOutputLines(p))
        p = self.cli.command('git --git-dir=%s/.git show %s' % (self.projPath, self.proj['commit_branch']), True)
        out = self.cli.getOutputLines(p)
        outMatch = re.search('^commit\s(\w+)', '\n'.join(out))
        cmTmp = []
        if outMatch is not None:
            commit = outMatch.group(1)
            cmTmp.append(commit)
        startComFound = False
        for c in cmTmp:
            if comFilter is None:
                if c == self.proj['comstart']:
                    startComFound = True
            else:
                if c == comFilter:
                    startComFound = True    
            if startComFound:
                commits.append(c)
        if rollback is None:
            return commits
        else:
            return commits[-int(rollback):]
    
    def getCommitForTag(self, tag):
        p = self.cli.command('git --git-dir=%s/.git fetch --tags' % (self.projPath), True)
        self.cli.printOutput(self.cli.getOutputLines(p))
        p = self.cli.command('git --git-dir=%s/.git show %s' % (self.projPath, tag), True)
        out = self.cli.getOutputLines(p)
        outMatch = re.search('^commit\s(\w+)', '\n'.join(out))
        if outMatch is not None:
            commit = outMatch.group(1)
            return commit
        else:
            return None
    
    def checkout(self, element):
        print('Checking out: %s' % (element))
        p = self.cli.command('git --git-dir=%s/.git --work-tree=%s checkout -f %s' % (self.projPath, self.projPath, element), True)
        out = self.cli.getOutputLines(p, 'serr')
        self.cli.printOutput(out)
        
    def stash(self, restore=False):
        if restore is False:
            print('Stashing changes..')
            p = self.cli.command('git --git-dir=%s/.git stash' % (self.projPath), True)
        else:
            print('Restoring stash..')
            p = self.cli.command('git --git-dir=%s/.git stash apply' % (self.projPath), True)
        self.cli.printOutput(self.cli.getOutputLines(p))
    
class StaticaDb():
    def __init__(self, db):
        self.__dbPath = db
        self.__db = None
        
    def connect(self):
        self.__db = sqlite3.connect(self.__dbPath)
        return self.__db
    
    def init(self):
        db = self.connect()
        # Create tables
        initQ = 'CREATE TABLE file_hashmap (id INTEGER PRIMARY KEY, original_hash TEXT, current_hash TEXT, project TEXT, path TEXT UNIQUE)'
        self.query(initQ)
        initQ = 'CREATE TABLE project (id INTEGER PRIMARY KEY, name TEXT UNIQUE)'
        self.query(initQ)
        initQ = 'CREATE TABLE version (id INTEGER PRIMARY KEY, name TEXT UNIQUE, type TEXT, branch TEXT)'
        self.query(initQ)
        initQ = 'CREATE TABLE type (id INTEGER PRIMARY KEY, name TEXT UNIQUE)'
        self.query(initQ)
        initQ = '''CREATE TABLE issue (id INTEGER PRIMARY KEY, proj_id INTEGER, ver_id INTEGER, type_id INTEGER, func TEXT, class TEXT, path TEXT,
                    FOREIGN KEY(proj_id) REFERENCES project(id),
                    FOREIGN KEY(ver_id) REFERENCES version(id),
                    FOREIGN KEY(type_id) REFERENCES type(id))'''
        self.query(initQ)
        initQ = '''CREATE TABLE issue_state (id INTEGER PRIMARY KEY, project TEXT, version TEXT, type TEXT, func TEXT, class TEXT, path TEXT, state INTEGER)'''
        self.query(initQ)
        initQ = '''CREATE TABLE project_to_version (id INTEGER PRIMARY KEY, proj_id INTEGER, ver_id INTEGER)'''
        self.query(initQ)
        initQ = '''CREATE TABLE parse_metadata (id INTEGER PRIMARY KEY, proj_id INTEGER, ver_id INTEGER, duration_sec REAL, file_list TEXT, total_files INTEGER, total_lines REAL, parsed_lines REAL, skipped_lines REAL, utime INTEGER, errors BLOB,
                    FOREIGN KEY(proj_id) REFERENCES project(id),
                    FOREIGN KEY(ver_id) REFERENCES version(id))'''
        self.query(initQ)
        initQ = '''CREATE TABLE check_metadata (id INTEGER PRIMARY KEY, proj_id INTEGER, ver_id INTEGER, type_id INTEGER, duration_sec REAL, utime INTEGER,
                    FOREIGN KEY(proj_id) REFERENCES project(id),
                    FOREIGN KEY(ver_id) REFERENCES version(id),
                    FOREIGN KEY(type_id) REFERENCES type(id))'''
        self.query(initQ)
        
    def query(self, q, commit=True):
        q = q.strip()
        cur = self.__db.cursor()
        cur.execute(q)
        if re.search('^(create|insert|update|delete|replace)', q, re.IGNORECASE) is not None and commit is True:
            self.__db.commit()
        return cur
    
    def commit(self):
        self.__db.commit()
    
    def getTags(self, proj):
        tags = []
        print('Getting db tags..')
        res = self.getProjectVersions(proj, 'tag')
        for r in res:
            tags.append(r)
        return tags
    
    def getCommits(self, proj):
        commits = []
        print('Getting db commits..')
        res = self.getProjectVersions(proj, 'commit')
        for r in res:
            commits.append(r)
        return commits
    
    def getCheckMetadata(self, proj, vers):
        res = []
        q = '''SELECT cm.id, cm.proj_id, cm.ver_id, cm.type_id, cm.duration_sec, cm.utime, v.name AS ver, p.name AS proj, t.name AS type FROM check_metadata AS cm
                JOIN type AS t ON cm.type_id = t.id
                JOIN project AS p ON cm.proj_id = p.id
                JOIN version AS v ON cm.ver_id = v.id
                WHERE proj = '%s' AND ver = '%s\'''' % (proj, vers)
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def getParseMetadata(self, proj, vers):
        res = []
        q = '''SELECT pm.id, pm.proj_id, pm.ver_id, pm.duration_sec, pm.total_lines, pm.parsed_lines, pm.skipped_lines, pm.utime, pm.errors, pm.total_files, pm.file_list, v.name AS ver, p.name AS proj FROM parse_metadata AS pm
                JOIN project AS p ON pm.proj_id = p.id
                JOIN version AS v ON pm.ver_id = v.id
                WHERE proj = '%s' AND ver = '%s'
                ORDER BY utime DESC''' % (proj, vers)
        c = self.query(q)
        return c.fetchone()
    
    def getProjectVersions(self, proj, type):
        res = []
        q = '''SELECT DISTINCT ptv.ver_id, ptv.proj_id, v.name AS ver, p.name AS proj, v.type AS type FROM project_to_version AS ptv
                JOIN project AS p ON ptv.proj_id = p.id
                JOIN version AS v ON ptv.ver_id = v.id
                WHERE proj = '%s' AND type = '%s\'''' % (proj, type)
        c = self.query(q)
        for r in c:
            res.append(str(r[2]))
        return res
    
    def getProjectVersion(self, pID, vID):
        r = self.query('SELECT * FROM project_to_version WHERE proj_id = %d AND ver_id = %d' % (pID, vID))
        return r.fetchone()
    
    def getProject(self, name=None):
        if name is None:
            return self.getProjects()
        else:
            r = self.query('''SELECT * FROM project WHERE name = '%s\'''' % (name))
            return r.fetchone()
    
    def getProjects(self):
        res = []
        q = 'SELECT * FROM project'
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def getVersion(self, name=None):
        if name is None:
            return self.getVersions()
        else:
            r = self.query('''SELECT * FROM version WHERE name = '%s\'''' % (name))
            return r.fetchone()
    
    def getVersions(self):
        res = []
        q = 'SELECT * FROM version'
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def getType(self, name=None):
        if name is None:
            return self.getTypes()
        else:
            r = self.query('''SELECT * FROM type WHERE name = '%s\'''' % (name))
            return r.fetchone()
    
    def getTypes(self):
        res = []
        q = 'SELECT * FROM type'
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def getIssue(self, func, cls, path, type, strict=False, ver=None):
        res = []
        if strict is False:
            join = 'LEFT OUTER JOIN'
        else:
            join = 'JOIN'
        q = '''SELECT i.id, i.type_id, i.ver_id, i.func, i.class, i.path, t.name AS checktype, v.name AS ver, istate.state FROM issue AS i
                JOIN type AS t ON i.type_id = t.id
                JOIN version AS v ON i.ver_id = v.id
                JOIN project AS p ON i.proj_id = p.id
                %s issue_state AS istate ON (i.func = istate.func AND i.class = istate.class AND i.path = istate.path AND p.name = istate.project AND t.name = istate.type)
                WHERE i.func = '%s' AND i.class = '%s' AND i.path = '%s' AND t.name = '%s\'''' % (join, func, cls, path, type)
        if ver is not None:
            q += ''' AND v.name = '%s\'''' % (ver)
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def updateIssueStateData(self, proj, ver, func, cls, path, type, state, new=False):
        # Get db ids
        pRec = self.getProject(proj)
        tRec = self.getType(type)
        vRec = self.getVersion(ver)
        pID = pRec[1]
        vID = vRec[1]
        tID = tRec[1]
        state = int(state)
        if new:
            q = '''INSERT INTO issue_state (project, version, type, func, class, path, state) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d)''' % (pID, vID, tID, func, cls, path, state)
        else:
            q = '''UPDATE issue_state set state=%d WHERE project = '%s' AND version = '%s' AND type = '%s' AND func = '%s' AND class = '%s' AND path = '%s\'''' % (state, pID, vID, tID, func, cls, path)
        self.query(q)
    
    def getIssueData(self, proj, type='tag', version=None):
        res = []
        q = '''SELECT i.id, i.proj_id, i.ver_id, i.type_id, i.func, i.class, i.path, p.name AS project, v.name AS version, v.type AS version_type, t.name AS type, istate.state FROM issue AS i
                JOIN version AS v ON i.ver_id = v.id
                JOIN project AS p ON i.proj_id = p.id
                JOIN type AS t ON i.type_id = t.id
                LEFT OUTER JOIN issue_state AS istate ON (i.func = istate.func AND i.class = istate.class AND i.path = istate.path AND p.name = istate.project AND t.name = istate.type)
                WHERE p.name = '%s' AND version_type = '%s\'''' % (proj, type)
        if version is not None:
            q += ''' AND v.name = '%s\'''' % (version)
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def diffIssues(self, proj, version, check, mode='new', type='tag'):
        res = []
        cur = []
        old = []
        q1Lst = None
        q2Lst = None
        # Get version list
        vers = self.getProjectVersions(proj, type)
        mData = self.getParseMetadata(proj, version)
        mPaths = mData[10]
        paths = mPaths.split(',')
        stopVer = False
        for v in vers:
            if v == version:
                stopVer = True
            if stopVer is False or v == version:
                cur.append('"%s"' % (v))
            if stopVer is False:
                old.append('"%s"' % (v))
        if mode == 'new':
            q1Lst = ','.join(cur)
            q2Lst = ','.join(old)
        if mode == 'fix':
            q1Lst = ','.join(old)
            q2Lst = '"%s"' % (version)
          
        pathQ = []
        for p in paths:
            pathQ.append('"%s"' % (p))  
        pathStr = ','.join(pathQ)
        q = ''' SELECT i.func, i.class, i.path, p.name AS project, t.name AS type FROM issue AS i
                    JOIN version AS v ON i.ver_id = v.id
                    JOIN project AS p ON i.proj_id = p.id
                    JOIN type AS t ON i.type_id = t.id
                    LEFT OUTER JOIN issue_state AS istate ON (i.func = istate.func AND i.class = istate.class AND i.path = istate.path AND p.name = istate.project AND t.name = istate.type)
                    WHERE p.name = '%s' AND v.type = '%s' AND v.name IN (%s) AND t.name = '%s' AND i.path IN (%s)''' % (proj, type, q1Lst, check, pathStr)
        q2 = '''EXCEPT
                SELECT i.func, i.class, i.path, p.name AS project, t.name AS type FROM issue AS i
                    JOIN version AS v ON i.ver_id = v.id
                    JOIN project AS p ON i.proj_id = p.id
                    JOIN type AS t ON i.type_id = t.id
                    LEFT OUTER JOIN issue_state AS istate ON (i.func = istate.func AND i.class = istate.class AND i.path = istate.path AND p.name = istate.project AND t.name = istate.type)
                    WHERE p.name = '%s' AND v.type = '%s' AND v.name IN (%s) AND t.name = '%s' AND i.path IN (%s)''' % (proj, type, q2Lst, check, pathStr)
        q = q + q2
        c = self.query(q)
        for r in c:
            if r not in res:
                res.append((r[0], r[1], r[2]))
        return res
    
    def getFileHashmapData(self, proj):
        res = []
        q = '''SELECT * FROM file_hashmap WHERE project = '%s\'''' % (proj)
        c = self.query(q)
        for r in c:
            res.append(r)
        return res
    
    def updateFileHashmapData(self, path, ohash, nhash, proj, new=False):
        if new:
            q = '''INSERT INTO file_hashmap (original_hash, current_hash, project, path) VALUES ('%s', '%s', '%s', '%s')''' % (ohash, nhash, proj, path)
        else:
            q = '''UPDATE file_hashmap set current_hash='%s' WHERE project = '%s' AND path = '%s' AND current_hash = '%s\'''' % (nhash, proj, path, ohash)
        self.query(q)
    
class StaticaCli():
    
    def command(self, command, enableShell=None):
        try:
            #print("Executing local command: " + command)
            p = Popen(command, bufsize=-1, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=enableShell)
            return p
        except OSError as oe:
            print("[OSError]: Error executing local command: " + command + " Message: {0}".format(oe.strerror))
            traceback.print_exc()
            return None
    
    def writeInput(self, inputStream, data):
        return inputStream.communicate(data + "\n")
       
    def getOutput(self, proc):
        return proc.communicate()
    
    def getOutputLines(self, proc, type='sout'):
        lines = []
        data = self.getOutput(proc)
        if type is 'sout':
            data = data[0]
        if type is 'serr':
            data = data[1]
        if data is not None:
            data = data.splitlines()
            for line in data:
                line = line.rstrip()
                lines.append(line)
        return lines
    
    def getExitCode(self, proc):
        if proc is not None:
            proc.wait()
            return proc.returncode
        else:
            return None
        
    def printOutput(self, data):
        if data is not None:
            for line in data:
                line = line.rstrip()
                if line is not None:
                    print(line)
        else:
            #print("No data received for output")
            pass
        
class StaticaConfig():
    def __init__(self, conf=None):
        if conf is None:
            self.__confPath = MAIN_CONF
        else:
            self.__confPath = conf
        print('Welcome to Statica :: Using config: %s' % (self.__confPath))
        
    def __getProjSections(self, c):
        sections = []
        for s in c.sections():
            pMatch = re.match('\w+Project$', s)
            if pMatch is not None:
                proj = {}
                for i in c.items(s):
                    val = c.get(s, i[0])
                    if i[0] == 'standalone':
                        val = c.getboolean(s, i[0])
                    proj[i[0]] = val
                sections.append(proj)
        return sections
    
    def getPath(self):
        return self.__confPath
            
    def load(self):
        # Parse config file
        try:
            config = ConfigParser.ConfigParser()
            config.readfp(open(self.__confPath))
            if config.has_section('Global') is False:
                raise Exception('Missing global configuration')
            conf = {}
            conf['global'] = {}
            for i in config.items('Global'):
                conf['global'][i[0]] = i[1]
            conf['projects'] = self.__getProjSections(config)
            if len(conf['projects']) < 1:
                raise Exception('No projects defined')
            return conf
        except Exception as e:
            print('Exception reading config file: %s (%s)' % (self.__confPath, e))
            sys.exit(1)


def get_instance(project=None):
    return Statica(project)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--conf', help="Path to config file (default: conf/statica.conf)")
    parser.add_argument('--mode', help="Mode [(d)aemon|(s)ingle] (default single)")
    parser.add_argument('--report', help="Always show a report in single mode", action="store_true")
    parser.add_argument('--project', type=str, help="Only check specified project (must be defined in conf)")
    parser.add_argument('--start', type=str, help="Start from specific tag/commit (-[digit] to count back from last tag, <version> to check single version)")
    parser.add_argument('--reload', help="Refresh project data from git", action="store_true")
    parser.add_argument('--check', help="Only run specific checks, overrides config (ie: csrf,danger)")
    parser.add_argument('--errors', help="Show parsing errors", action="store_true")
    args = parser.parse_args()
    conf = args.conf
    report = args.report
    if args.mode is None:
        mode = 's'
    else:
        mode = args.mode
    s = Statica(args.project, conf, mode, report, args.start, args.reload, args.check, args.errors, False)
    s.run()
    try:
        while s.isRunning():
            pass
    except (KeyboardInterrupt, SystemExit):
        s.stop()
    
