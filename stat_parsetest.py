from statica import get_instance
from project import StaticaParser

from phply.phpast import Variable, Method
from phply.phpparse import make_parser

file = '/Applications/XAMPP/xamppfiles/htdocs/statica/concrete5/concrete/controllers/single_page/dashboard/users/points/actions.php'

sp = StaticaParser(make_parser(), file)
print('%s :: Total lines: %d' % (sp.path, sp.totalLines))
output = sp.parse()
print(sp.getErrors())
print('Skipped: %d' % (sp.removedLines))
print(output)
#print('\n'.join(sp.parseableLines))

s = get_instance('concrete5')
proj = s.proj
print(proj[0])

po = s.getProjectCheckObject('concrete5', '8.0.1')
ns = po.getUserInputs(output, [(Variable, '$_REQUEST', None), (Variable, '$_GET', None), (Variable, '$_POST', None)])
print(ns)
